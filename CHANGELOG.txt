0.0.5.37
45df8ca (HEAD -> main, tag: 0.0.5.37, team/main) update to 0.0.5.37
fbd78d3 reduce glitchiness in media display by removing redundant loads

0.0.5.36
eff37c8 (tag: 0.0.5.36) update to 0.0.5.36
5529904 update gradle and dependencies
b4823b8 more improvements for running in the background
a238bc8 handle local content URIs for local media, and improve overall media display
6ec1f6c enable background sync when app is in background
4f7742d (team/sdk2-1.1.5-update, dev/sdk2-1.1.5-update, sdk2-1.1.5-update) switch from GlobalScope to local coroutinescope
067b41c required kotlin coroutine updates for sdk2 changes
725db93 update sdk2 to 1.1.5

0.0.5.35
7d5b993 (tag: 0.0.5.35) update to version 0.0.5.35
1405b9f make sure to hide everything for reaction messages that are invisible

0.0.5.34
8aa11fc (tag: 0.0.5.34, dev/main) update to 0.0.5.34
f586ba8 Merge branch 'main' of gitlab.com:keanuapp/keanu-android-trinity-app into main
c7759a2 Fixed crash because of accidental code copy.
977f462 Issue #28: Improve notifications: Collect all messages of a room in one notification, update and remove messages when needed.
affe29e Issue #28: Update notification when user edits message. Cancel notification, when user deletes message.
5371d3e Merge branch 'main' of gitlab.com:keanuapp/keanu-android-trinity-app into main
c001b7a Issue #26: Renamed misnamed `Theme` (inherited from the iOS version) to way more talking `Router` as talked on the last call.
370e345 Merge branch 'main' of gitlab.com:keanuapp/keanu-android-trinity-app into main
88f2299 Updated some dependencies which were easy to do.
c62badc Merge branch 'main' of gitlab.com:keanuapp/keanu-android-trinity-app into main
c76e557 reduce password length size here - this is temp fix for legacy passwords that were more like pins for some users
cac091a show version number
ed5ade5 Updated some dependencies which were easy to do.
17664ab Fixed crash because of wrong context. Cannot show alert on application context.

7360176 (tag: 0.0.5.33) update app-actual depends to match applib
10641d2 small change in converting File to Uri
526aed4 more improvements to "just in time" timeline listening for rooms with activity
bd65c10 fix media forward, export, download function
392f90e properly show the number of reactions as text
a1e6b2e only get/sync timelines when needed, and fix bug with notifications

5c4298a (tag: 0.0.5.32) update to 0.0.5.32
3dad192 startSync in the Service and only init Timeline once a room is opened

174ee9d (tag: 0.0.5.31) update to 0.0.5.31
4e159c0 improve display of room selector activity
eb2d83f just use foreground sync
7c07dda update dependencies
1baf38d fix media forwarding, export and share
dc014ea disable nearby sharing icon for now
ee18446 fix image export and download to use FileProvider
33284a4 don't show "0" next to reaction if there are no senders (only your local reaction)
52117de update matisse picker to version with latest fixex

