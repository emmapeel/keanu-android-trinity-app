package info.guardianproject.keanu.core

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.webkit.MimeTypeMap
import info.guardianproject.keanu.core.service.ImServiceConstants
import info.guardianproject.keanu.core.service.RemoteImService
import info.guardianproject.keanu.core.ui.auth.AuthenticationActivity
import info.guardianproject.keanu.core.ui.auth.RecaptchaActivity
import info.guardianproject.keanu.core.ui.auth.TermsActivity
import info.guardianproject.keanu.core.ui.auth.TermsActivityArgument
import info.guardianproject.keanu.core.ui.chatlist.ChooseRoomActivity
import info.guardianproject.keanu.core.ui.friends.AddFriendActivity
import info.guardianproject.keanu.core.ui.friends.FriendActivity
import info.guardianproject.keanu.core.ui.friends.FriendsPickerActivity
import info.guardianproject.keanu.core.ui.gallery.GalleryActivity
import info.guardianproject.keanu.core.ui.me.settings.camouflage.AppIconChooserActivity
import info.guardianproject.keanu.core.ui.onboarding.OnboardingActivity
import info.guardianproject.keanu.core.ui.room.RoomActivity
import info.guardianproject.keanu.core.ui.room.StoryActivity
import info.guardianproject.keanu.core.ui.widgets.VideoViewActivity
import info.guardianproject.keanu.core.util.SoundService
import info.guardianproject.keanu.core.verification.*
import info.guardianproject.keanuapp.ui.LockScreenActivity
import info.guardianproject.keanuapp.ui.PanicSetupActivity
import info.guardianproject.keanuapp.ui.ServicesActivity
import info.guardianproject.keanuapp.ui.StickerActivity
import info.guardianproject.keanuapp.ui.accounts.AccountSettingsActivity
import info.guardianproject.keanuapp.ui.camera.CameraActivity
import info.guardianproject.keanuapp.ui.contacts.DevicesActivity
import info.guardianproject.keanuapp.ui.contacts.GroupDisplayActivity
import info.guardianproject.keanuapp.ui.conversation.AddUpdateMediaActivity
import info.guardianproject.keanuapp.ui.legacy.SettingActivity
import info.guardianproject.keanuapp.ui.lock.AppLockScreenActivity
import info.guardianproject.keanuapp.ui.qr.QrScanActivity
import info.guardianproject.keanuapp.ui.stories.StoryEditorActivity
import info.guardianproject.keanuapp.ui.stories.StoryGalleryActivity
import info.guardianproject.keanuapp.ui.widgets.ImageViewActivity
import info.guardianproject.keanuapp.ui.widgets.MultipleImageSelectionActivity
import info.guardianproject.keanuapp.ui.widgets.PdfViewActivity
import info.guardianproject.keanuapp.ui.widgets.WebViewActivity
import java.util.*

open class Router {

    companion object {
        val instance = Router()
    }

    open val remoteServiceClass
        get() = RemoteImService::class.java

    open val soundServiceClass
        get() = SoundService::class.java

    open val routerClass
        get() = RouterActivity::class.java

    open val onboardingClass
        get() = OnboardingActivity::class.java

    open val mainClass
        get() = MainActivity::class.java

    open val friendClass
        get() = FriendActivity::class.java

    open val addFriendClass
        get() = AddFriendActivity::class.java

    open val chooseRoomClass
        get() = ChooseRoomActivity::class.java

    open val friendsPickerClass
        get() = FriendsPickerActivity::class.java

    open val settingsClass
        get() = SettingActivity::class.java

    open val lockScreenClass
        get() = AppLockScreenActivity::class.java

    open val roomClass
        get() = RoomActivity::class.java

    open val storyClass
        get() = StoryActivity::class.java

    open val storyEditorClass
        get() = StoryEditorActivity::class.java

    open val newDeviceClass
        get() = NewDeviceActivity::class.java

    open val manualCompareClass
        get() = ManualCompareActivity::class.java

    open val verificationClass
        get() = VerificationActivity::class.java

    open val intrusionClass
        get() = IntrusionActivity::class.java

    open val devicesClass
        get() = DevicesActivity::class.java

    open val authenticationClass
        get() = AuthenticationActivity::class.java

    open val recaptchaClass
        get() = RecaptchaActivity::class.java

    open val termsClass
        get() = TermsActivity::class.java

    open val imageViewClass
        get() = ImageViewActivity::class.java

    open val cameraClass
        get() = CameraActivity::class.java

    open val pdfViewClass
        get() = PdfViewActivity::class.java

    open val webViewClass
        get() = WebViewActivity::class.java

    open val videoViewClass
        get() = VideoViewActivity::class.java

    open val galleryClass
        get() = GalleryActivity::class.java

    open val stickerClass
        get() = StickerActivity::class.java

    open val servicesClass
        get() = ServicesActivity::class.java

    open val qrScanClass
        get() = QrScanActivity::class.java

    open val groupDisplayClass
        get() = GroupDisplayActivity::class.java

    open val addUpdateMediaClass
        get() = AddUpdateMediaActivity::class.java

    open val multipleImageSelectionClass
        get() = MultipleImageSelectionActivity::class.java

    open val storyGalleryClass
        get() = StoryGalleryActivity::class.java

    open val panicSetupClass
        get() = PanicSetupActivity::class.java

    open val appIconChooserClass
        get() = AppIconChooserActivity::class.java

    open val accountSettingsClass
        get() = AccountSettingsActivity::class.java


    @JvmOverloads
    open fun remoteService(context: Context, checkAutoLogin: Boolean? = null): Intent {
        val i = Intent(context, remoteServiceClass)

        if (checkAutoLogin != null) {
            i.putExtra(ImServiceConstants.EXTRA_CHECK_AUTO_LOGIN, checkAutoLogin)
        }

        return i
    }

    @JvmOverloads
    open fun soundService(context: Context, soundUri: String? = null): Intent {
        val i = Intent(context, soundServiceClass)

        if (soundUri != null) {
            i.action = SoundService.ACTION_START_PLAYBACK
            i.putExtra(SoundService.EXTRA_SOUND_URI, soundUri)
        }
        else {
            i.action = SoundService.ACTION_STOP_PLAYBACK
        }

        return i
    }

    @JvmOverloads
    open fun router(context: Context, data: Uri? = null, type: String? = null, lock: Boolean? = null): Intent {
        val i = Intent(context, routerClass)

        if (data != null) {
            i.action = Intent.ACTION_SEND

            if (type != null) {
                i.setDataAndTypeAndNormalize(data, type)
            }
            else {
                i.data = data
            }
        }
        else if (type != null) {
            i.action = Intent.ACTION_SEND

            i.setTypeAndNormalize(type)
        }
        else if (lock == true) {
            i.action = RouterActivity.ACTION_LOCK_APP
        }

        return i
    }

    open fun onboarding(context: Context): Intent {
        return Intent(context, onboardingClass)
    }

    @JvmOverloads
    open fun main(context: Context, inviteUserId: String? = null, joinRoomId: String? = null,
                  openRoomId: String? = null, isFirstTime: Boolean? = null, preselectTab: Int? = null): Intent
    {
        val i = Intent(context, mainClass)

        if (inviteUserId != null) {
            i.putExtra(MainActivity.EXTRA_INVITE_USER_ID, inviteUserId)
        }

        if (joinRoomId != null) {
            i.putExtra(MainActivity.EXTRA_JOIN_ROOM_ID, joinRoomId)
        }

        if (openRoomId != null) {
            i.putExtra(MainActivity.EXTRA_OPEN_ROOM_ID, openRoomId)
        }

        if (isFirstTime != null) {
            i.putExtra(MainActivity.EXTRA_IS_FIRST_TIME, isFirstTime)
        }

        if (preselectTab != null) {
            i.putExtra(MainActivity.EXTRA_PRESELECT_TAB, preselectTab)
        }

        return i
    }

    @JvmOverloads
    open fun friend(context: Context, userId: String? = null, showSendPrivateMsg: Boolean? = true): Intent {
        val friendActivityIntent = Intent(context, friendClass)

        if (userId != null) {
            friendActivityIntent.putExtra(FriendActivity.EXTRA_USER_ID, userId)
        }
        friendActivityIntent.putExtra(FriendActivity.EXTRA_SHOW_SEND_PRIVATE_MSG, showSendPrivateMsg)

        return friendActivityIntent
    }

    @JvmOverloads
    open fun addFriend(context: Context, username: String? = null, addLocalFriend: Boolean? = null): Intent {
        val i = Intent(context, addFriendClass)

        if (username != null) {
            i.putExtra(AddFriendActivity.EXTRA_USERNAME, username)
        }

        if (addLocalFriend != null) {
            i.putExtra(AddFriendActivity.EXTRA_ADD_LOCAL_FRIEND, addLocalFriend)
        }

        return i
    }

    open fun chooseRoom(context: Context): Intent {
        return Intent(context, chooseRoomClass)
    }

    @JvmOverloads
    open fun friendsPicker(context: Context, usernames: ArrayList<String?>? = null): Intent {
        val i = Intent(context, friendsPickerClass)

        if (usernames != null) {
            i.putExtra(FriendsPickerActivity.EXTRA_EXCLUDED_CONTACTS, usernames)
        }

        return i
    }

    open fun settings(context: Context): Intent {
        return Intent(context, settingsClass)
    }

    @JvmOverloads
    open fun lockScreen(context: Context, changePassphrase: Boolean = false): Intent {
        val i = Intent(context, lockScreenClass)

        if (changePassphrase) {
            i.action = LockScreenActivity.ACTION_CHANGE_PASSPHRASE
        }

        return i
    }

    @JvmOverloads
    open fun room(context: Context, roomId: String? = null, showRoomInfo: Boolean? = null): Intent {
        val i = Intent(context, roomClass)

        if (roomId != null) {
            i.putExtra(RoomActivity.EXTRA_ROOM_ID, roomId)
        }

        if (showRoomInfo != null) {
            i.putExtra(RoomActivity.EXTRA_SHOW_ROOM_INFO, showRoomInfo)
        }

        return i
    }

    @JvmOverloads
    open fun story(context: Context, roomId: String? = null, showRoomInfo: Boolean? = null, contributorMode: Boolean? = null): Intent {
        val i = Intent(context, storyClass)

        if (roomId != null) {
            i.putExtra(RoomActivity.EXTRA_ROOM_ID, roomId)
        }

        if (showRoomInfo != null) {
            i.putExtra(RoomActivity.EXTRA_SHOW_ROOM_INFO, showRoomInfo)
        }

        if (contributorMode != null) {
            i.putExtra(StoryActivity.EXTRA_CONTRIBUTOR_MODE, contributorMode)
        }

        return i
    }

    open fun storyEditor(context: Context): Intent {
        return Intent(context, storyEditorClass)
    }

    @JvmOverloads
    open fun newDevice(context: Context, userId: String? = null, deviceId: String? = null): Intent {
        val i = Intent(context, newDeviceClass)

        if (userId != null) {
            i.putExtra(VerificationManager.EXTRA_USER_ID, userId)
        }

        if (deviceId != null) {
            i.putExtra(VerificationManager.EXTRA_DEVICE_ID, deviceId)
        }

        return i
    }

    @JvmOverloads
    open fun manualCompare(context: Context, userId: String? = null, deviceId: String? = null): Intent {
        val i = Intent(context, manualCompareClass)

        if (userId != null) {
            i.putExtra(VerificationManager.EXTRA_USER_ID, userId)
        }

        if (deviceId != null) {
            i.putExtra(VerificationManager.EXTRA_DEVICE_ID, deviceId)
        }

        return i
    }

    open fun verification(context: Context): Intent {
        return Intent(context, verificationClass)
    }

    open fun intrusion(context: Context): Intent {
        return Intent(context, intrusionClass)
    }

    @JvmOverloads
    open fun devices(context: Context, userId: String? = null): Intent {
        val i = Intent(context, devicesClass)

        if (userId != null) {
            i.putExtra(DevicesActivity.EXTRA_USER_ID, userId)
        }

        return i
    }

    @JvmOverloads
    open fun authentication(context: Context, isSignUp: Boolean? = null, userId: String? = null, password: String? = null): Intent {
        val i = Intent(context, authenticationClass)

        if (isSignUp != null) {
            i.putExtra(AuthenticationActivity.EXTRA_IS_SIGN_UP, isSignUp)
        }

        if (userId != null) {
            i.putExtra(AuthenticationActivity.EXTRA_USER_ID, userId)
        }

        if (password != null) {
            i.putExtra(AuthenticationActivity.EXTRA_PASSWORD, password)
        }

        return i
    }

    @JvmOverloads
    open fun recaptcha(context: Context, publicKey: String? = null, homeServer: String? = null): Intent {
        val i = Intent(context, recaptchaClass)

        if (publicKey != null) {
            i.putExtra(RecaptchaActivity.EXTRA_PUBLIC_KEY, publicKey)
        }

        if (homeServer != null) {
            i.putExtra(RecaptchaActivity.EXTRA_HOME_SERVER, homeServer)
        }

        return i
    }

    @JvmOverloads
    open fun terms(context: Context, terms: TermsActivityArgument? = null): Intent {
        val i = Intent(context, termsClass)

        if (terms != null) {
            i.putExtra(TermsActivity.EXTRA_TERMS, terms)
        }

        return i
    }

    @JvmOverloads
    open fun imageView(context: Context, files: ArrayList<Uri>? = null, mimeTypes: ArrayList<String>? = null,
                       currentIndex: Int? = null, showResend: Boolean? = null, showForward: Boolean? = null): Intent
    {
        val i = Intent(context, imageViewClass)

        if (files != null) {
            i.putExtra(ImageViewActivity.EXTRA_URIS, files)

            if (mimeTypes != null) {
                i.putExtra(ImageViewActivity.EXTRA_MIME_TYPES, mimeTypes)
            }
            else {
                i.putExtra(ImageViewActivity.EXTRA_MIME_TYPES, ArrayList(files.map {
                    MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(it.toString()))
                }))
            }

            if (currentIndex != null) {
                i.putExtra(ImageViewActivity.EXTRA_CURRENT_INDEX, currentIndex)
            }
        }

        if (showResend != null) {
            i.putExtra(ImageViewActivity.EXTRA_SHOW_RESEND, showResend)
        }

        showForward?.let { i.putExtra(ImageViewActivity.EXTRA_SHOW_FORWARD, showForward) }

        return i
    }

    @JvmOverloads
    open fun camera(context: Context, oneAndDone: Boolean? = null): Intent {
        val i = Intent(context, cameraClass)

        if (oneAndDone != null) {
            i.putExtra(CameraActivity.SETTING_ONE_AND_DONE, oneAndDone)
        }

        return i
    }

    @JvmOverloads
    open fun pdfView(context: Context, data: Uri? = null, type: String? = null): Intent {
        val i = Intent(context, pdfViewClass)

        if (data != null) {
            if (type != null) {
                i.setDataAndTypeAndNormalize(data, type)
            }
            else {
                i.data = data
            }
        }
        else if (type != null) {
            i.setTypeAndNormalize(type)
        }

        return i
    }

    @JvmOverloads
    open fun webView(context: Context, data: Uri? = null, type: String? = null, eventId: String? = null): Intent {
        val i = Intent(context, webViewClass)

        if (data != null) {
            if (type != null) {
                i.setDataAndTypeAndNormalize(data, type)
            }
            else {
                i.data = data
            }
        }
        else if (type != null) {
            i.setTypeAndNormalize(type)
        }

        if (eventId != null) {
            i.putExtra(WebViewActivity.EXTRA_EVENT_ID, eventId)
        }

        return i
    }

    @JvmOverloads
    open fun videoView(context: Context, data: Uri? = null, type: String? = null): Intent {
        val i = Intent(context, videoViewClass)

        if (data != null) {
            if (type != null) {
                i.setDataAndTypeAndNormalize(data, type)
            }
            else {
                i.data = data
            }
        }
        else if (type != null) {
            i.setTypeAndNormalize(type)
        }

        return i
    }

    open fun gallery(context: Context): Intent {
        return Intent(context, galleryClass)
    }

    open fun sticker(context: Context): Intent {
        return Intent(context, stickerClass)
    }

    open fun services(context: Context): Intent {
        return Intent(context, servicesClass)
    }

    @JvmOverloads
    open fun qrScan(context: Context, text: String? = null, mimeType: String? = null, finishOnFirst: Boolean = false, getRaw: Boolean = false): Intent {
        val i = Intent(context, qrScanClass)

        if (text != null) {
            i.putExtra(Intent.EXTRA_TEXT, text)
        }

        if (mimeType != null) {
            i.setTypeAndNormalize(mimeType)
        }

        i.putExtra(QrScanActivity.EXTRA_FINISH_ON_FIRST, finishOnFirst)
        i.putExtra(QrScanActivity.EXTRA_GET_RAW, getRaw)

        return i
    }

    @JvmOverloads
    open fun groupDisplay(context: Context, roomId: String? = null): Intent {
        val i = Intent(context, groupDisplayClass)

        if (roomId != null) {
            i.putExtra(RoomActivity.EXTRA_ROOM_ID, roomId)
        }

        return i
    }

    @JvmOverloads
    open fun addUpdateMedia(context: Context, title: String? = null): Intent {
        val i = Intent(context, addUpdateMediaClass)

        if (title != null) {
            i.putExtra(Intent.EXTRA_TITLE, title)
        }

        return i
    }

    @JvmOverloads
    open fun multipleImageSelection(context: Context, title: String? = null): Intent {
        val i = Intent(context, multipleImageSelectionClass)

        if (title != null) {
            i.putExtra(Intent.EXTRA_TITLE, title)
        }

        return i
    }

    @JvmOverloads
    open fun storyGallery(context: Context, galleryMode: Int? = null): Intent {
        val i = Intent(context, storyGalleryClass)

        if (galleryMode != null) {
            i.putExtra(StoryGalleryActivity.ARG_GALLERY_MODE, galleryMode)
        }

        return i
    }

    open fun panicSetup(context: Context): Intent {
        return Intent(context, panicSetupClass)
    }

    open fun accountSettings(context: Context): Intent {
        val i = Intent(context, accountSettingsClass)

        return i
    }
}