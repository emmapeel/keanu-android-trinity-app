package info.guardianproject.keanu.core.service

import android.content.pm.PackageManager
import android.os.Build
import androidx.core.os.ConfigurationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.api.session.pushers.HttpPusher
import timber.log.Timber

class KeanuFirebaseMessagingService: FirebaseMessagingService() {

    private val session
        get() = ImApp.sImApp?.matrixSession


    override fun onNewToken(token: String) {
        val session = session ?: return
        val deviceId = session.sessionParams.deviceId ?: return
        val locale = ConfigurationCompat.getLocales(resources.configuration)[0] ?: return

        val pm = packageManager

        val appInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            pm.getApplicationInfo(packageName, PackageManager.ApplicationInfoFlags.of(0))
        }
        else {
            @Suppress("DEPRECATION")
            pm.getApplicationInfo(packageName, 0)
        }

        val pusher = HttpPusher(
            pushkey = token,
            appId = packageName,
            profileTag = "mobile_${session.myUserId.hashCode()}",
            lang = locale.language,
            appDisplayName = pm.getApplicationLabel(appInfo).toString(),
            deviceDisplayName = deviceId,
            url = "https://${getString(R.string.default_server)}/_matrix/push/v1/notify",
            enabled = true,
            deviceId = deviceId,
            append = false,
            withEventIdOnly = true)

        session.pushersService().enqueueAddHttpPusher(pusher)

        Timber.d("#onNewToken: pusher=%s", pusher)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        Timber.d("#onMessageReceived: message=%s", message)

        // TODO: Is this already enough? Isn't session already started and started syncing?
        //   Wait for ops to set up FCM / Sygnal push service.
    }
}