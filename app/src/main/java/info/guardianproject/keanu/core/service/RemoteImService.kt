package info.guardianproject.keanu.core.service

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanu.core.ui.mention.TextPillsUtils
import info.guardianproject.keanuapp.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.crypto.attachments.toElementToDecrypt
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.file.FileService
import org.matrix.android.sdk.api.session.getRoom
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.model.message.getFileUrl
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.session.room.timeline.Timeline
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.room.timeline.isEdition
import timber.log.Timber
import java.util.*

open class RemoteImService : Service(), Observer<List<RoomSummary>> {

    private var mIsFirstTime = true

    private var mStatusBarNotifier: StatusBarNotifier? = null

    private val mApp: ImApp?
        get() = application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private val mRoomNotifyMap = HashMap<String?, Boolean>()

    private val mNotifyStartDate = Date().time

    private lateinit var preferenceProvider: PreferenceProvider

    companion object {

        private val ALLOWED_ROOM_TYPES = listOf(
            EventType.MESSAGE,
            EventType.STICKER,
            EventType.REACTION,
            EventType.TYPING,
            EventType.RECEIPT,
            EventType.STATE_ROOM_NAME,
            EventType.STATE_ROOM_TOPIC,
            EventType.STATE_ROOM_MEMBER,
            EventType.REDACTION
        )

        /**
         * Standard filter for event lists. Will drop all events, which are
         *
         * - a redaction (The closest thing to a deletion, we can have in Matrix.)
         * - not of any type listed in `ALLOWED_ROOM_TYPES`.
         * - an edition of another event (Matrix SDK will show the edition on the original event, typically, anyway.)
         *
         * You would typically use this as predicate in a [Iterable.filter] call in a [Timeline.Listener]!
         *
         * @return true, if ok, false if it should be discarded.
         */
        val filterEvents = { te: TimelineEvent ->
            // This is ordered in perceived weight of executed code!
            te.root.getClearType().let { t -> ALLOWED_ROOM_TYPES.any { it == t } }
                    && !te.isEdition()
        }

        var instance: RemoteImService? = null

        private const val notifyId = 7

        private val mCoroutineScope: CoroutineScope by lazy {
            CoroutineScope(Dispatchers.IO)
        }

        fun downloadMedia(session: Session?, mc: MessageWithAttachmentContent) {
            if (session?.fileService()?.isFileInCache(mc) == false) {
                val fs = session.fileService()

                if (fs.fileState(mc) != FileService.FileState.Downloading) {
                    mCoroutineScope.launch {
                        val url = mc.getFileUrl()

                        if (url?.startsWith("mxc") == true) {
                            try {
                                fs.downloadFile(
                                    mc.getFileName(), mc.mimeType, url,
                                    mc.encryptedFileInfo?.toElementToDecrypt()
                                )
                            } catch (throwable: Throwable) {
                                throwable.printStackTrace()
                            }
                        }
                    }
                }
            }
        }
    }

    private val mForegroundNotification by lazy {
        var flags = PendingIntent.FLAG_UPDATE_CURRENT

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            flags = flags or PendingIntent.FLAG_IMMUTABLE
        }

        NotificationCompat.Builder(this, KeanuConstants.NOTIFICATION_CHANNEL_ID_SERVICE)
            .setContentTitle(getString(R.string.app_name))
            .setSmallIcon(R.drawable.notify_app)
            .setOngoing(true)
            .setAutoCancel(false)
            .setWhen(System.currentTimeMillis())
            .setContentText(getString(R.string.app_unlocked))
            .setContentIntent(
                PendingIntent.getActivity(
                    this,
                    0,
                    mStatusBarNotifier?.getDefaultIntent(),
                    flags
                )
            )
            .build()
    }


    override fun onCreate() {
        super.onCreate()
        preferenceProvider = PreferenceProvider(applicationContext)

        Timber.d("ImService started")

        mStatusBarNotifier = StatusBarNotifier(this)

        initService()
    }

    private fun initService() {
        instance = this
    }

    private var liveRoomSumData: LiveData<List<RoomSummary>>? = null

    open fun initNotificationListener() {

        if (liveRoomSumData == null) {
            val builder = RoomSummaryQueryParams.Builder()
            builder.memberships = listOf(Membership.JOIN, Membership.INVITE)

            liveRoomSumData = mSession?.roomService()?.getRoomSummariesLive(builder.build())

            liveRoomSumData?.observeForever(this)

            initRoomNotificationOnListener()
        }
    }

    private fun initRoomNotificationOnListener() {
        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN, Membership.INVITE)
        val roomSums = mSession?.roomService()?.getRoomSummaries(builder.build())

        for (roomSum in roomSums ?: emptyList()) {
            val room = mSession?.getRoom(roomSum.roomId)

            mRoomNotifyMap[roomSum.roomId] = false

            room?.roomPushRuleService()?.getLiveRoomNotificationState()?.observeForever { state: RoomNotificationState ->
                mRoomNotifyMap[room.roomId] = state == preferenceProvider.getNotificationState()
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        if (mIsFirstTime) {
            startForeground(notifyId, mForegroundNotification)

            mIsFirstTime = false
        }

        if (intent?.getBooleanExtra("notify", true) == true) {
            initNotificationListener()
        }

        if (ImServiceConstants.EXTRA_CHECK_SHUTDOWN == intent?.action) {
            stopSelf()
        }

        return START_REDELIVER_INTENT
    }

    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    override fun onLowMemory() {
        Timber.d("onLowMemory()!")
    }

    /**
     * Release memory when the UI becomes hidden or when system resources become low.
     * @param level the memory-related event that was raised.
     */
    override fun onTrimMemory(level: Int) {
        Timber.d("OnTrimMemory: $level")
    }

    fun cancelNotification(id: String) {
        mStatusBarNotifier?.cancelNotification(id)
    }

    override fun onDestroy() {
        super.onDestroy()
        liveRoomSumData?.removeObserver(this)
        liveRoomSumData = null
    }

    override fun onChanged(value: List<RoomSummary>) {
        for (summary in value) {
            // Ignore, when there's no unread messages.
            if (!summary.hasUnreadMessages && (!summary.hasNewMessages)) continue

            // Ignore, when the event should be discarded.
            val te = summary.latestPreviewableEvent ?: continue
            if (!filterEvents(te)) continue

            // Ignore, when our user sent this.
            if (mSession?.myUserId == te.senderInfo.userId) continue

            // Ignore, when notifications for room are disabled.
            if (mRoomNotifyMap.containsKey(te.roomId) && mRoomNotifyMap[te.roomId] == false) continue

            val mc = te.getLastMessageContent()
            if (mc is MessageWithAttachmentContent) downloadMedia(mSession, mc)
            val preferenceProvider = mApp?.preferenceProvider
            if (preferenceProvider?.isNotificationEnable() == true) {
                val hasMention = isMyselfMentioned(
                    te
                )
                if (preferenceProvider.getNotificationState() == RoomNotificationState.ALL_MESSAGES ||
                    preferenceProvider.getNotificationState() == RoomNotificationState.MENTIONS_ONLY && hasMention
                ) {
                    if (te.root.originServerTs!! > mNotifyStartDate)
                        mStatusBarNotifier?.notifyChat(te)
                }

            }


        }
    }

    private fun isMyselfMentioned(tEvent: TimelineEvent): Boolean {

        try {
            val messageContent = tEvent.getLastMessageContent()
            if (messageContent is MessageTextContent && messageContent.formattedBody != null
                && messageContent.formattedBody?.contains("mx-reply") == true
            ) {
                // have reply content
                messageContent.formattedBody?.let {

                    val document = Jsoup.parse(it)
                    val mentionPart = document.select(TextPillsUtils.MENTION_TAG).last()

                    val userIdLink = mentionPart?.attributes()?.get("href") ?: ""
                    val idStartIndex = userIdLink.lastIndexOf("@")
                    val mentionedUserId =
                        if (idStartIndex >= 0 && idStartIndex < userIdLink.length) {
                            userIdLink.substring(idStartIndex)
                        } else ""

                    return mentionedUserId == mSession?.myUserId
                }
            }
        } catch (exception: Exception) {
            Timber.tag("RemoteIMService").e(exception.toString())
        }

        return false
    }
}

