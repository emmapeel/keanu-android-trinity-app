package info.guardianproject.keanu.core.type

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.view.inputmethod.EditorInfoCompat
import androidx.core.view.inputmethod.InputConnectionCompat
import androidx.core.view.inputmethod.InputContentInfoCompat
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider

class CustomTypefaceEditText : AppCompatEditText {

    var themeColorText = -1

    private var mInit = false

    private var onReachContentSelect: OnReachContentSelect? = null


    interface OnReachContentSelect {
        fun onReachContentClick(inputContentInfoCompat: InputContentInfoCompat)
    }


    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    constructor(context: Context) : super(context) {
        init()
    }


    private fun init() {
        if (!mInit) {
            val preferenceProvider = PreferenceProvider(context)
            themeColorText = preferenceProvider.getHeaderColor()
            val t = CustomTypefaceManager.getCurrentTypeface(context)
            if (t != null) typeface = t
            mInit = true
        }

        if (themeColorText > 0 || themeColorText < -1) setTextColor(themeColorText)
    }

    fun setReachContentClickListener(onReachContentSelect: OnReachContentSelect?) {
        this.onReachContentSelect = onReachContentSelect
    }

    override fun setText(text: CharSequence, type: BufferType) {
        super.setText(text, type)

        if (themeColorText > 0 || themeColorText < -1) setTextColor(themeColorText)
    }

    override fun onCreateInputConnection(editorInfo: EditorInfo): InputConnection? {
        val ic = super.onCreateInputConnection(editorInfo) ?: return null

        EditorInfoCompat.setContentMimeTypes(editorInfo, arrayOf("image/gif"))

        val callback =
            InputConnectionCompat.OnCommitContentListener { inputContentInfo: InputContentInfoCompat, flags: Int, _: Bundle? ->
                // Read and display inputContentInfo asynchronously.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1 && flags and
                    InputConnectionCompat.INPUT_CONTENT_GRANT_READ_URI_PERMISSION != 0
                ) {
                    try {
                        inputContentInfo.requestPermission()
                    } catch (e: Exception) {
                        return@OnCommitContentListener false // return false if failed
                    }
                }

                onReachContentSelect?.onReachContentClick(inputContentInfo)

                true // return true if succeeded
            }

        return InputConnectionCompat.createWrapper(ic, editorInfo, callback)
    }
}