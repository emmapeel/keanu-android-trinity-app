package info.guardianproject.keanu.core.type;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider;

public class CustomTypefaceTextView extends androidx.appcompat.widget.AppCompatTextView {

    boolean mInit = false;
    int themeColorText = -1;

    public CustomTypefaceTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }


    public CustomTypefaceTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }


    public CustomTypefaceTextView(Context context) {
        super(context);

        init();
    }


    private void init() {

        if (!mInit) {
            Typeface t = CustomTypefaceManager.getCurrentTypeface(getContext());

            if (t != null)
                setTypeface(t);

            final PreferenceProvider preferenceProvider = new PreferenceProvider(getContext());
            themeColorText = preferenceProvider.getTextColor();

            mInit = true;
        }

        if (themeColorText > 0 || themeColorText < -1)
            setTextColor(themeColorText);

    }


    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);

        if (themeColorText > 0 || themeColorText < -1)
            setTextColor(themeColorText);

    }


}