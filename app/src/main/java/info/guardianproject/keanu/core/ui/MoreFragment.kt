package info.guardianproject.keanu.core.ui

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.MainActivity
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentMoreBinding

class MoreFragment : Fragment() {

    private val mApp
        get() = activity?.application as? ImApp

    private lateinit var mBinding: FragmentMoreBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        super.onCreateView(inflater, container, savedInstanceState)

        mBinding = FragmentMoreBinding.inflate(inflater, container, false)

        mBinding.btnOpenGallery.setOnClickListener {
            context?.let { it.startActivity(mApp?.router?.gallery(it)) }
        }

        mBinding.btnOpenServices.setOnClickListener { openServices() }

        mBinding.btnOpenGroups.setOnClickListener {
            (activity as? MainActivity)?.createRoom()
        }

        mBinding.btnOpenStickers.setOnClickListener {
            context?.let { it.startActivity(mApp?.router?.sticker(it)) }
        }

        mBinding.btnOpenThemes.setOnClickListener { showColors() }

        return mBinding.root
    }

    private fun showColors() {
        val context = context ?: return

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)

        ColorPickerDialogBuilder
                .with(context)
                .setTitle(getString(R.string.title_choosse_color))
                .initialColor(prefs.getInt(Preferences.THEME_COLOR_HEADER, -1))
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .lightnessSliderOnly()
                .setOnColorSelectedListener { }
                .setPositiveButton(getString(R.string.ok)) { _: DialogInterface?, selectedColor: Int, _: Array<Int?>? ->
                    prefs.edit().putInt(Preferences.THEME_COLOR_HEADER, selectedColor).apply()

                    (activity as? MainActivity)?.applyStyle()
                }
                .setNegativeButton(getString(R.string.cancel), null)
                .build()
                .show()
    }

    private fun openServices() {
        context?.let { it.startActivity(mApp?.router?.services(it)) }
    }
}