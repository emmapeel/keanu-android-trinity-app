package info.guardianproject.keanu.core.ui

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentPasswordDialogBinding

@Suppress("unused")
class PasswordDialogFragment(private val style: Style = Style.ENTRY,
                             private val title: String? = null,
                             private val message: String? = null,
                             private val cancelLabel: String? = null,
                             private val okLabel: String? = null,
                             private val completion: ((PasswordDialogFragment) -> Unit)? = null) : DialogFragment(), DialogInterface.OnShowListener, View.OnClickListener {

    enum class Style {
        ENTRY, CONFIRM, NEW_PASSWORD
    }

    val oldPassword: String?
        get() = if (isGood) mBinding.etOldPassword.text.toString() else null

    val password: String?
        get() = if (isGood) mBinding.etPassword.text.toString() else null

    private var isGood = false

    private lateinit var mBinding: FragmentPasswordDialogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Catch taps outside the dialog, which cancel the dialog. Consumers might need to update
        // their UI.
        dialog?.setOnCancelListener { completion?.invoke(this) }

        return super.onCreateView(inflater, container, savedInstanceState)
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(activity)

        builder.setTitle(title)
        builder.setMessage(message)

        mBinding = FragmentPasswordDialogBinding.inflate(layoutInflater, null, false)
        builder.setView(mBinding.root)

        if (style != Style.NEW_PASSWORD) (mBinding.etOldPassword.parent as? View)?.visibility = View.GONE

        if (style == Style.ENTRY) (mBinding.etConfirm.parent as? View)?.visibility = View.GONE

        builder.setNegativeButton(cancelLabel ?: getString(android.R.string.cancel)) { _, _ ->
            completion?.invoke(this)
        }

        builder.setPositiveButton(okLabel ?: getString(android.R.string.ok), null)

        val dialog = builder.create()

        dialog.setOnShowListener(this) // Workaround, so the dialog stays open, when passwords are invalid.

        return dialog
    }

    /**
     * Instead of connecting a {@link DialogInterface.OnClickListener} which
     * only fires, after the dialog is gone, we connect a
     * {@link View.OnClickListener} directly to the button, which fires
     * <b>before</b> the dialog is gone, which enables us to prevent the closing
     * of it.
     */
    override fun onShow(dialog: DialogInterface?) {
        (this.dialog as? AlertDialog)?.getButton(AlertDialog.BUTTON_POSITIVE)?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        mBinding.etOldPassword.error = null
        mBinding.etPassword.error = null
        mBinding.etConfirm.error = null

        if (style == Style.NEW_PASSWORD && TextUtils.isEmpty(mBinding.etOldPassword.text)) {
            mBinding.etOldPassword.error = getString(R.string.This_field_may_not_be_empty_)
            mBinding.etOldPassword.requestFocus()
            return
        }

        if (TextUtils.isEmpty(mBinding.etPassword.text)) {
            mBinding.etPassword.error = getString(R.string.This_field_may_not_be_empty_)
            mBinding.etPassword.requestFocus()
            return
        }

        //TODO improve this, but yes some users have passwords less than 8 chars
        if (mBinding.etPassword.text?.length ?: 0 < 4) {
            mBinding.etPassword.error = getString(R.string.Password_is_too_short_)
            mBinding.etPassword.requestFocus()
            return
        }

        if (style != Style.ENTRY) {
            if (TextUtils.isEmpty(mBinding.etConfirm.text)) {
                mBinding.etConfirm.error = getString(R.string.This_field_may_not_be_empty_)
                mBinding.etConfirm.requestFocus()
                return
            }

            if (mBinding.etConfirm.text.toString() != mBinding.etPassword.text.toString()) {
                mBinding.etConfirm.error = getString(R.string.Passwords_don_t_match_)
                mBinding.etConfirm.requestFocus()
                return
            }
        }

        isGood = true

        dialog?.dismiss()

        // Let this thing close, first before continuing execution flow.
        Handler(Looper.getMainLooper()).postDelayed({
            completion?.invoke(this)
        }, 100)
    }
}