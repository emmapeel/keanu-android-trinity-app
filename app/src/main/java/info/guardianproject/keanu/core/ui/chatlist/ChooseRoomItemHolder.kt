package info.guardianproject.keanu.core.ui.chatlist

import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanuapp.databinding.ChooseRoomItemBinding

class ChooseRoomItemHolder(private val mBinding: ChooseRoomItemBinding) : RecyclerView.ViewHolder(mBinding.root) {
    val line1 get() = mBinding.line1
    val avatar get() = mBinding.avatar
}
