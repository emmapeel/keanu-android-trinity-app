package info.guardianproject.keanu.core.ui.friends

import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityContactBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.user.model.User

class FriendActivity : BaseActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_USER_ID = "user-id"
        const val EXTRA_SHOW_SEND_PRIVATE_MSG = "extra_show_send_private_msg"
    }

    private val mApp
        get() = application as? ImApp

    private var mUserId: String = ""

    private var mUser: User? = null

    private lateinit var mBinding: ActivityContactBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityContactBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        setSupportActionBar(mBinding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeActionContentDescription(R.string.action_done)

        mUserId = intent.getStringExtra(EXTRA_USER_ID) ?: ""
        val showSendPrivateMsgOption = intent.getBooleanExtra(EXTRA_SHOW_SEND_PRIVATE_MSG, true)
        if (!showSendPrivateMsgOption) {
            mBinding.btnAddAsFriend.visibility = View.GONE
        }

        val session = mApp?.matrixSession
        mUser = session?.userService()?.getUser(mUserId)

        title = ""
        mBinding.tvName.text = mUser?.displayName ?: mUserId
        mBinding.tvUsername.text = mUserId

        try {
            val avatarUrl = session?.contentUrlResolver()?.resolveThumbnail(mUser?.avatarUrl, 120, 120, ThumbnailMethod.SCALE)

            GlideUtils.loadImageFromUri(this, Uri.parse(avatarUrl), mBinding.imgAvatar, true)

            mBinding.imgAvatar.visibility = View.VISIBLE
            mBinding.imageSpacer.visibility = View.GONE
        }
        catch (ignored: Exception) {}

        mBinding.btnStartChat.setOnClickListener(this)

        mBinding.btnAddAsFriend.setOnClickListener(this)

        mBinding.btnViewDevicesKeys.setOnClickListener(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v) {
            mBinding.btnStartChat, mBinding.btnAddAsFriend -> {
                startActivity(mApp?.router?.main(this@FriendActivity, inviteUserId = mUserId))

                finish()
            }

            mBinding.btnViewDevicesKeys -> {
                startActivity(mApp?.router?.devices(this@FriendActivity, userId = mUserId))
            }
        }
    }
}