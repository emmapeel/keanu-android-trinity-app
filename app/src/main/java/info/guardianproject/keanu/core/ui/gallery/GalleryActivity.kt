package info.guardianproject.keanu.core.ui.gallery

import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.webkit.MimeTypeMap
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanu.core.util.PrettyTime
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.WrapContentGridLayoutManager
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.AwesomeActivityGalleryBinding
import info.guardianproject.keanuapp.databinding.GalleryItemViewBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.*

class GalleryActivity : BaseActivity() {

    private val mApp
        get() = application as? ImApp

    private val mSession
        get() = mApp?.matrixSession

    private lateinit var mBinding: AwesomeActivityGalleryBinding

    private val mHandler: Handler = Handler(Looper.getMainLooper())
    private val numberOfColumns = 2

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = AwesomeActivityGalleryBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        setTitle(R.string.photo_gallery)

        updateUi()

        setupRecyclerView(mBinding.recyclerView)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        applyStyleForToolbar()

    }

    override fun applyStyleForToolbar() {


        //not set color
        val preferenceProvider = PreferenceProvider(applicationContext)
        val selColor = preferenceProvider.getHeaderColor()

        if (selColor != -1) {
            window.navigationBarColor = selColor
            window.statusBarColor = selColor

            supportActionBar?.setBackgroundDrawable(ColorDrawable(selColor))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            R.id.menu_message_nearby -> {
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_gallery, menu)
        return true
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager =
            WrapContentGridLayoutManager(recyclerView.context, numberOfColumns)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.padding4)
        recyclerView.addItemDecoration(
            GridSpacingItemDecoration(
                numberOfColumns,
                spacingInPixels,
                true,
                0,
                false
            )
        )

        val query = RoomSummaryQueryParams.Builder()
        query.memberships = listOf(Membership.INVITE, Membership.JOIN)

        val adapter = GalleryAdapter(this, mHandler)
        recyclerView.adapter = adapter

        mCoroutineScope.launch {
            mSession?.roomService()?.getRoomSummaries(query.build())?.forEach {
                val room = mSession?.roomService()?.getRoom(it.roomId) ?: return@forEach

                room.timelineService().getAttachmentMessages().forEach loop@ { event ->
                    val content = event.getLastMessageContent() as? MessageWithAttachmentContent ?: return@loop
                    try {
                        val duration =
                            ((event.root.content?.get("info") as? Map<*, *>)?.getOrDefault(
                                "duration",
                                0.0
                            ) as? Double)?.toLong() ?: 0
                        val file = mSession?.fileService()?.downloadFile(content)
                        adapter.add(
                            event.root.originServerTs,
                            event.senderInfo.displayName,
                            Uri.fromFile(file),
                            duration
                        )

                        lifecycleScope.launch {
                            updateUi()
                        }
                    } catch (failure: Throwable) {
                        Timber.d(failure)
                    }
                }
            }
        }
    }

    private fun updateUi() {
        val hasNoMedia = mBinding.recyclerView.adapter?.itemCount == 0

        mBinding.recyclerView.visibility = if (hasNoMedia) View.GONE else View.VISIBLE
        mBinding.emptyView.visibility = if (hasNoMedia) View.VISIBLE else View.GONE
        mBinding.emptyViewImage.visibility = mBinding.emptyView.visibility
    }

    class GalleryAdapter(context: GalleryActivity?, handler: Handler) :
        RecyclerView.Adapter<GalleryMediaViewHolder>(),
        GalleryMediaViewHolder.GalleryMediaViewHolderListener {
        private val mContext = WeakReference(context)

        private var mUriMap = HashMap<Long?, Attachment>()
        private var mFiles = ArrayList<Attachment>()
        private var mHandler = handler

        fun add(ts: Long?, sender: String?, file: Uri, duration: Long?) {
            mUriMap[ts] = Attachment(sender, file, ts, duration)

            val mUriMap2 = HashMap(mUriMap)

            val newFiles =
                ArrayList(mUriMap2.toSortedMap(compareByDescending { it }).values)
            var position = newFiles.size - 1

            for (i in 0 until mFiles.size) {
                if (mFiles[i] != newFiles[i]) {
                    position = i
                    break
                }
            }

            mFiles = newFiles

            mHandler.post {
                notifyItemInserted(position)
            }
        }

        override fun getItemCount(): Int {
            return mFiles.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryMediaViewHolder {
            val binding =
                GalleryItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)

            return GalleryMediaViewHolder(binding.root, parent.context)
        }

        override fun onBindViewHolder(holder: GalleryMediaViewHolder, position: Int) {
            val currentAttachment = mFiles[position]
            val galleryDate = currentAttachment.time?.let { Date(it) }

            holder.bind(
                getMimeType(currentAttachment.file),
                currentAttachment.file.toString(),
                currentAttachment.sender,
                PrettyTime.format(galleryDate, mContext.get()),
                currentAttachment.duration
            )

            holder.setListener(this)
        }

        override fun onImageClicked(viewHolder: GalleryMediaViewHolder, image: Uri) {
            val context = mContext.get() ?: return
            val mediaFiles = ArrayList<Uri>()

            mFiles.forEach { gallery -> mediaFiles.add(gallery.file) }

            context.startActivity(
                context.mApp?.router?.imageView(
                    context, mediaFiles,
                    currentIndex = 0.coerceAtLeast(mediaFiles.indexOf(image))
                )
            )
        }

        private fun getMimeType(file: Uri): String? {
            return MimeTypeMap.getSingleton()
                .getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(file.toString()))
        }
    }
}