package info.guardianproject.keanu.core.ui.me

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.widget.addTextChangedListener
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.ImageChooserHelper
import info.guardianproject.keanu.core.util.KeyboardUtils
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.constants.BundleKeys
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityEditProfileBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session
import java.io.File


class EditProfileActivity : BaseActivity() {

    private lateinit var mBinding: ActivityEditProfileBinding
    private lateinit var mTvToolbarAction: TextView
    private lateinit var mProgressBarLoading: ProgressBar
    private var mImageChooserHelper: ImageChooserHelper? = null
    private val mSession: Session?
        get() = (application as? ImApp)?.matrixSession
    private lateinit var mPickAvatar: ActivityResultLauncher<Intent>
    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(
            Dispatchers.IO + SnackbarExceptionHandler(
                mBinding.root,
                object : SnackbarExceptionHandler.ExceptionCallback {
                    override fun onHandleException(exception: Throwable) {
                        mProgressBarLoading.visibility = View.GONE
                    }

                })
        )
    }


    private var mRequestCamera =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            mImageChooserHelper?.intent?.let { mPickAvatar.launch(it) }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val helper = ImageChooserHelper(this)
        mImageChooserHelper = helper
        mBinding = ActivityEditProfileBinding.inflate(layoutInflater)
        mPickAvatar = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            helper.getCallback(this::setAvatar)
        )
        setContentView(mBinding.root)

        supportActionBar?.hide()
        mBinding.toolbar.tvBackText.setOnClickListener {
            finish()
        }

        val editTextDisplayName = mBinding.editTextDisplayName
        intent.getStringExtra(BundleKeys.BUNDLE_KEY_DISPLAY_NAME)?.let { displayName ->
            editTextDisplayName.setText(displayName)
        }
        intent.getStringExtra(BundleKeys.BUNDLE_KEY_IMAGE_NAME)?.let { userImageUrl ->
            GlideUtils.loadImageFromUri(
                this,
                Uri.parse(userImageUrl),
                mBinding.ivAvatar,
                false
            )

        }

        mBinding.textInputLayoutDisplayName.setEndIconOnClickListener {
            editTextDisplayName.setText("")
        }
        mBinding.editImageView.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.CAMERA
                    )
                ) {
                    Snackbar.make(mBinding.root, R.string.grant_perms, Snackbar.LENGTH_LONG).show()
                } else {
                    mRequestCamera.launch(Manifest.permission.CAMERA)
                }
            } else {
                mImageChooserHelper?.intent?.let { mPickAvatar.launch(it) }
            }
        }

        editTextDisplayName.setOnFocusChangeListener { view, hasFocus ->
            if (!hasFocus) {
                KeyboardUtils.hideKeyboard(this@EditProfileActivity, view)
            }
        }

        mProgressBarLoading = mBinding.pbLoading

        mTvToolbarAction = mBinding.toolbar.tvActionRight
        mTvToolbarAction.setOnClickListener {
            mCoroutineScope.launch {
                runOnUiThread {
                    mProgressBarLoading.visibility = View.VISIBLE
                }
                mSession?.profileService()?.setDisplayName(mSession?.myUserId ?: "", editTextDisplayName.text.toString())
                runOnUiThread {
                    mProgressBarLoading.visibility = View.GONE
                    finish()
                }
            }
        }

        editTextDisplayName.addTextChangedListener {
            val newTextLength = it?.length ?: 0
            if (newTextLength > 0) {
                mTvToolbarAction.visibility = View.VISIBLE
            } else {
                mTvToolbarAction.visibility = View.GONE
            }
        }

    }

    private fun setAvatar(bmp: Bitmap) {
        mBinding.ivAvatar.setImageDrawable(BitmapDrawable(resources, bmp))

        val userId = mSession?.myUserId ?: return

        mCoroutineScope.launch {
            @Suppress("BlockingMethodInNonBlockingContext")
            val file = File.createTempFile("avatar", "jpg")

            file.outputStream().use { os ->
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, os)
            }

            try {
                mSession?.profileService()?.updateAvatar(userId, Uri.fromFile(file), file.name)
            } finally {
                file.delete()
            }
        }
    }
}