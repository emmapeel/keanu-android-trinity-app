package info.guardianproject.keanu.core.ui.me.providers

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import androidx.preference.PreferenceManager
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import java.util.*

class PreferenceProvider(applicationContext: Context) {
    private val PREF_KEY_ENABLE_DISGUISE = "pref_key_enable_disguise"
    private val PREF_KEY_DISGUISE_ICON_ID = "pref_key_disguise_icon_id"
    private val passwordSharedPref = "pref_tmppass"
    private val debugEnabled = "pref_debugInfo"
    private val screenshotEnabled = "pref_screenshot"
    private val notificationEnabled = "pref_notification"
    private val notificationState = "pref_notificationState"
    private val notificationSound = "pref_notification_sound"
    private val notificationVibrate = "pref_notification_vibrate"
    private val notificationRingtoneUri = "pref_notification_ringtone"
    private val mentionNotificationSound = "pref_mention_notification_sound"
    private val mentionNotificationVibrate = "pref_mention_notification_vibrate"
    private val mentionNotificationRingtoneUri = "pref_mention_notification_ringtone"
    private val defaultNotificationRingtoneUri = "content://settings/system/notification_sound"
    private val startOnBoot = "pref_start_on_boot"
    private val headerColor = "pref_header_color"
    private val textColor = "pref_text_color"
    private val backgroundColor = "pref_background_color"
    private val language = "pref_language"
    private val appPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(applicationContext)
    }

    /**
     * Remove Password
     */
    fun removePassword() {
        with(appPreferences.edit()) {
            remove(passwordSharedPref)
            apply()
        }
    }

    /**
     * Change Application Icons
     */
    var disguiseId: Int
        get() = appPreferences.getInt(PREF_KEY_DISGUISE_ICON_ID, 0)
        set(disguiseId) {
            with(appPreferences.edit()) {
                putInt(PREF_KEY_DISGUISE_ICON_ID, disguiseId)
                apply()
            }
        }

    fun isEnableDisguise(): Boolean {
        return appPreferences.getBoolean(PREF_KEY_ENABLE_DISGUISE, false)
    }

    fun setEnableDisguise(enable: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(PREF_KEY_ENABLE_DISGUISE, enable)
            apply()
        }
    }

    /**
     * Send debug info
     */
    fun getDebugLogInfo(): Boolean {
        return appPreferences.getBoolean(debugEnabled, false)
    }

    fun setDebugLogInfo(isEnabled: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(debugEnabled, isEnabled)
            apply()
        }
    }

    fun getScreenshotInfo(): Boolean {
        return appPreferences.getBoolean(screenshotEnabled, false)
    }

    fun setScreenshotInfo(isEnabled: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(screenshotEnabled, isEnabled)
            apply()
        }
    }

    fun isNotificationEnable(): Boolean {
        return appPreferences.getBoolean(notificationEnabled, false)
    }

    fun setNotificationEnable(isEnabled: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(notificationEnabled, isEnabled)
            apply()
        }
    }

    fun getNotificationState(): RoomNotificationState {
        val notificationType =
            appPreferences.getString(notificationState, RoomNotificationState.ALL_MESSAGES.name)
        return RoomNotificationState.valueOf(notificationType!!)
    }

    fun setNotificationState(roomNotificationState: RoomNotificationState) {
        with(appPreferences.edit()) {
            putString(notificationState, roomNotificationState.name)
            apply()
        }
    }

    /**
     * All Notification Settings
     */
    fun isNotificationVibrate(): Boolean {
        return appPreferences.getBoolean(notificationVibrate, true)
    }

    fun setNotificationVibrate(isEnabled: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(notificationVibrate, isEnabled)
            apply()
        }
    }

    fun isNotificationSound(): Boolean {
        return appPreferences.getBoolean(notificationSound, true)
    }

    fun setNotificationSound(isEnabled: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(notificationSound, isEnabled)
            apply()
        }
    }

    fun getNotificationRingtone(): Uri {
        return Uri.parse(
            appPreferences.getString(
                notificationRingtoneUri,
                defaultNotificationRingtoneUri
            )
        )
    }

    fun setNotificationSound(uriString: String) {
        val ringtoneUri = uriString.ifEmpty { defaultNotificationRingtoneUri }
        with(appPreferences.edit()) {
            putString(notificationRingtoneUri, ringtoneUri)
            apply()
        }
    }

    /**
     * Mention Notification Settings
     */
    fun isMentionNotificationVibrate(): Boolean {
        return appPreferences.getBoolean(mentionNotificationVibrate, true)
    }

    fun setMentionNotificationVibrate(isEnabled: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(mentionNotificationVibrate, isEnabled)
            apply()
        }
    }

    fun isMentionNotificationSound(): Boolean {
        return appPreferences.getBoolean(mentionNotificationSound, true)
    }

    fun setMentionNotificationSound(isEnabled: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(mentionNotificationSound, isEnabled)
            apply()
        }
    }

    fun getMentionNotificationRingtone(): Uri {
        return Uri.parse(
            appPreferences.getString(
                mentionNotificationRingtoneUri,
                defaultNotificationRingtoneUri
            )
        )
    }

    fun setMentionNotificationSound(uriString: String) {
        val ringtoneUri = uriString.ifEmpty { defaultNotificationRingtoneUri }
        with(appPreferences.edit()) {
            putString(mentionNotificationRingtoneUri, ringtoneUri)
            apply()
        }
    }

    /**
     * startOnBoot
     */
    fun startOnBoot(): Boolean {
        return appPreferences.getBoolean(startOnBoot, false)

    }

    fun setStartOnBoot(isEnabled: Boolean) {
        with(appPreferences.edit()) {
            putBoolean(startOnBoot, isEnabled)
            apply()
        }
    }

    /**
     * Color Settings
     */
    fun getHeaderColor(): Int {
        return appPreferences.getInt(headerColor, -1)

    }

    fun setHeaderColor(color: Int) {
        with(appPreferences.edit()) {
            putInt(headerColor, color)
            apply()
        }
    }

    fun getTextColor(): Int {
        return appPreferences.getInt(textColor, -1)

    }

    fun setTextColor(color: Int) {
        with(appPreferences.edit()) {
            putInt(textColor, color)
            apply()
        }
    }

    fun getBackgroundColor(): Int {
        return appPreferences.getInt(backgroundColor, -1)

    }

    fun setBackgroundColor(color: Int) {
        with(appPreferences.edit()) {
            putInt(backgroundColor, color)
            apply()
        }
    }

    fun getLanguage(): String {
        return appPreferences.getString(language, Locale.getDefault().language)!!
    }

    fun setLanguage(code: String) {
        with(appPreferences.edit()) {
            putString(language, code)
            apply()
        }
    }


}