package info.guardianproject.keanu.core.ui.me.settings

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanu.core.util.Languages
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentSettingPreferencesBinding
import timber.log.Timber

class SettingPreferencesFragment : Fragment() {

    private lateinit var mBinding: FragmentSettingPreferencesBinding
    private lateinit var preferenceProvider: PreferenceProvider
    private var colorType = 0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentSettingPreferencesBinding.inflate(inflater, container, false)
        preferenceProvider = PreferenceProvider(requireContext())
        (activity as SettingsActivity).setBackButtonText(getString(R.string.profile_preferences))
        mBinding.layoutLanguage.setOnClickListener {
            showLanguageDialog()

        }
        mBinding.layoutHeaderColor.setOnClickListener {
            colorType = 0
            displayColorDialogBuilder(resources.getString(R.string.color_header_title))

        }
        mBinding.layoutTextColor.setOnClickListener {
            colorType = 1
            displayColorDialogBuilder(resources.getString(R.string.color_text_title))
        }
        mBinding.layoutBackgroundColor.setOnClickListener {
            colorType = 2
            displayColorDialogBuilder(resources.getString(R.string.color_bg_title))
        }
        setThemeColors()
        mBinding.layoutResetColors.setOnClickListener {
            preferenceProvider.setHeaderColor(-1)
            preferenceProvider.setTextColor(-1)
            preferenceProvider.setBackgroundColor(-1)
            setThemeColors()

        }
        mBinding.layoutCloseApp.setOnClickListener {
            requireActivity().finish()

        }
        mBinding.sbTuning.isChecked = preferenceProvider.startOnBoot()
        mBinding.sbTuning.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setStartOnBoot(isChecked)
        }

        return mBinding.root
    }

    private fun displayColorDialogBuilder(title: String) {
        val currentColor = when (colorType) {
            0 -> preferenceProvider.getHeaderColor()
            1 -> preferenceProvider.getTextColor()
            else -> preferenceProvider.getBackgroundColor()
        }
        ColorPickerDialogBuilder
            .with(requireContext())
            .setTitle(title)
            .initialColor(currentColor)
            .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
            .density(12)
            .setOnColorSelectedListener {
                Toast.makeText(requireContext(), "0x" + Integer.toHexString(it), Toast.LENGTH_SHORT)
                    .show()
            }
            .setPositiveButton(
                resources.getString(R.string.ok)
            ) { dialog, selectedColor, allColors ->
                setThemeColor(selectedColor)
                dialog.dismiss()
            }
            .setNegativeButton(
                resources.getString(R.string.cancel)
            ) { p0, _ ->
                Timber.d("Cancelled")
                p0?.dismiss()
            }
            .showColorEdit(true)
            .build()
            .show()
    }

    private fun setThemeColor(color: Int) {
        when (colorType) {
            0 -> {
                mBinding.ivHeaderColor.circleBackgroundColor = color
                preferenceProvider.setHeaderColor(color)
            }
            1 -> {
                mBinding.ivTextColor.circleBackgroundColor = color
                preferenceProvider.setTextColor(color)
            }
            else -> {
                mBinding.ivBackgroundColor.circleBackgroundColor = color
                preferenceProvider.setBackgroundColor(color)
            }

        }

    }

    private fun setThemeColors() {
        if (preferenceProvider.getHeaderColor() == -1) {

            mBinding.ivHeaderColor.circleBackgroundColor =
                ContextCompat.getColor(requireContext(), R.color.app_primary)
        } else {
            mBinding.ivHeaderColor.circleBackgroundColor = preferenceProvider.getHeaderColor()
        }
        if (preferenceProvider.getTextColor() == -1) {
            mBinding.ivTextColor.circleBackgroundColor =
                ContextCompat.getColor(requireContext(), R.color.text_color)
        } else {
            mBinding.ivTextColor.circleBackgroundColor = preferenceProvider.getTextColor()
        }
        if (preferenceProvider.getBackgroundColor() == -1) {
            mBinding.ivBackgroundColor.circleBackgroundColor =
                ContextCompat.getColor(requireContext(), R.color.border_color)
        } else {
            mBinding.ivBackgroundColor.circleBackgroundColor =
                preferenceProvider.getBackgroundColor()
        }


    }

    private fun showLanguageDialog() {
        val languages = Languages.get(requireActivity())
        AlertDialog.Builder(requireActivity())
            .setTitle(resources.getString(R.string.choose_language))
            .setSingleChoiceItems(
                languages.allNames, -1
            ) { dialogInterface, index ->
                val selectedLanguage = languages.allNames[index]
                ImApp.resetLanguage(requireActivity(), selectedLanguage)
                dialogInterface?.dismiss()
            }
            .setNegativeButton(
                resources.getString(R.string.cancel)
            ) { dialogInterface, _ -> dialogInterface?.dismiss() }
            .create()
            .show()

    }


}