package info.guardianproject.keanu.core.ui.me.settings

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import info.guardianproject.keanu.core.model.settings.SettingScreen
import info.guardianproject.keanu.core.util.constants.BundleKeys
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivitySettingsBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import java.lang.RuntimeException


class SettingsActivity : BaseActivity() {

    private lateinit var mBinding: ActivitySettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivitySettingsBinding.inflate(layoutInflater)

        setContentView(mBinding.root)

        window.apply {
            statusBarColor = Color.TRANSPARENT
        }

        supportActionBar?.hide()
        mBinding.toolbar.tvBackText.setOnClickListener {
            finish()
        }

        var settingFragment: Fragment? = null
        when(intent.extras?.get(BundleKeys.BUNDLE_KEY_SETTING_SCREEN)) {

            SettingScreen.NOTIFICATIONS -> {
                settingFragment = SettingNotificationFragment()
            }

            SettingScreen.PREFERENCES -> {
                settingFragment = SettingPreferencesFragment()

            }

            SettingScreen.ACCOUNT -> {
                settingFragment = SettingAccountFragment()
            }

            SettingScreen.SECURITY_PRIVACY -> {
                settingFragment = SettingSecurityPrivacyFragment()
            }

            SettingScreen.PHYSICAL_SAFETY -> {
                settingFragment = SettingPhysicalSafetyFragment()
            }

            SettingScreen.PHOTOS_VIDEOS -> {
                settingFragment = SettingPhotosVideosFragment()
            }

            SettingScreen.ABOUT -> {
                settingFragment = SettingAboutFragment()
            }

            SettingScreen.FEEDBACK -> {
                settingFragment = SettingFeedbackFragment()
            }
            else -> throw RuntimeException("Set settingFragment to corresponding fragment for: "
                    + intent.extras?.get(BundleKeys.BUNDLE_KEY_SETTING_SCREEN))
        }

        settingFragment?.let { supportFragmentManager.beginTransaction().replace(R.id.fragment_settings, settingFragment).commit() }
    }

    fun setBackButtonText(title: String) {
        mBinding.toolbar.tvBackText.text = title
    }
}