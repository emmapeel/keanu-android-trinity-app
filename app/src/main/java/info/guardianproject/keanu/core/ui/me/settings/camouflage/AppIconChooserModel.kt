package info.guardianproject.keanu.core.ui.me.settings.camouflage

data class AppIconChooserModel(val packageName: String, val appIconRes: Int, val appIconNameRes: Int)
