package info.guardianproject.keanu.core.ui.room

import android.content.Context
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.api.provider.RoomDisplayNameFallbackProvider

class KeanuRoomDisplayNameFallbackProvider(private val mContext: Context) :
    RoomDisplayNameFallbackProvider {


    override fun getNameForRoomInvite(): String {
        return mContext.getString(R.string.notify_groupchat_label)
    }

    override fun getNameForEmptyRoom(isDirect: Boolean, leftMemberNames: List<String>): String {
        return mContext.getString(R.string.default_group_title)
    }

    override fun getNameFor1member(name: String): String {
        return mContext.getString(R.string.default_group_title)
    }

    override fun getNameFor2members(name1: String, name2: String): String {
        return mContext.getString(R.string.room_displayname_two_members, name1, name2)
    }

    override fun getNameFor3members(name1: String, name2: String, name3: String): String {
        return mContext.getString(R.string.room_displayname_3_members, name1, name2, name3)
    }

    override fun getNameFor4members(
        name1: String,
        name2: String,
        name3: String,
        name4: String
    ): String {
        return mContext.getString(R.string.room_displayname_4_members, name1, name2, name3, name4)
    }

    override fun getNameFor4membersAndMore(
        name1: String,
        name2: String,
        name3: String,
        remainingCount: Int
    ): String {
        return mContext.resources.getQuantityString(R.plurals.room_displayname_four_and_more_members,
                remainingCount, name1, name2, name3, remainingCount)
    }
}