package info.guardianproject.keanu.core.ui.room

import android.Manifest.permission.*
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.media.AudioManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.*
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.material.snackbar.Snackbar
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.MainActivity
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanu.core.ui.quickresponse.QuickResponseFragment
import info.guardianproject.keanu.core.ui.quickresponse.QuickResponseLongClickListener
import info.guardianproject.keanu.core.util.AttachmentHelper
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.extensions.uris
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityRoomBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.widgets.ShareRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.crypto.attachments.toElementToDecrypt
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.model.message.getFileUrl
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.room.timeline.getTextEditableContent
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.util.*


open class RoomActivity : BaseActivity(), QuickResponseLongClickListener {

    companion object {
        const val EXTRA_ROOM_ID = "room-id"
        const val EXTRA_SHOW_ROOM_INFO = "show-room-info"

        const val REQUEST_ADD_MEDIA = 666
    }

    var roomId: String = ""
        private set

    private var mMediaRecorder: MediaRecorder? = null
    private var mAudioFilePath: File? = null
    var isAudioRecording = false


    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler((this as? RoomActivity)?.root))
    }

    private val mApp: ImApp?
        get() = application as? ImApp

    var backButtonHandler: View.OnClickListener? = null

    lateinit var mBinding: ActivityRoomBinding

    open val root: ViewGroup
        get() = mBinding.root

    open val toolbar
        get() = mBinding.toolbar

    open val mainContent: ViewGroup
        get() = mBinding.mainContent

    open val inputLayout: ViewGroup
        get() = mBinding.inputLayout

    open val historyView
        get() = mBinding.history

    open val btJumpToBottom
        get() = mBinding.btJumpToBottom

    open val composeMessage
        get() = mBinding.composeMessage

    open val btnSend
        get() = mBinding.btnSend

    open val btnMic
        get() = mBinding.btnMic

    open val btnDeleteVoice
        get() = mBinding.btnDeleteVoice

    open val btnAttach
        get() = mBinding.btnAttach

    open val attachPanel
        get() = mBinding.attachPanel

    open val btnAttachSticker
        get() = mBinding.btnAttachSticker

    open val btnAttachPicture
        get() = mBinding.btnAttachPicture

    open val btnTakePicture
        get() = mBinding.btnTakePicture

    open val btnAttachAudio
        get() = mBinding.btnAttachAudio

    open val btnAttachFile
        get() = mBinding.btnAttachFile

    open val btnCreateStory
        get() = mBinding.btnCreateStory

    open val audioRecordView
        get() = mBinding.recordView

    open val typingView
        get() = mBinding.tvTyping

    open val stickerBox
        get() = mBinding.stickerBox

    open val stickerPager
        get() = mBinding.stickerPager

    open val mediaPreviewCancel
        get() = mBinding.mediaPreviewCancel

    open val joinGroupView
        get() = mBinding.joinGroupView

    open val btnJoinAccept
        get() = mBinding.btnJoinAccept

    open val btnJoinDecline
        get() = mBinding.btnJoinDecline

    open val roomJoinTitle
        get() = mBinding.roomJoinTitle

    open val mediaPreviewContainer
        get() = mBinding.mediaPreviewContainer

    open val mediaPreview
        get() = mBinding.mediaPreview

    protected open val roomView: RoomView by lazy {
        RoomView(this, roomId)
    }

    val roomViewModel: RoomViewModel by viewModels()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityRoomBinding.inflate(layoutInflater)
        setContentView(root)

        roomViewModel.messageAction.observe(this) {

            when (it) {
                MessageAction.ADD_REACTION -> roomView.showQuickReactionsPopup(
                    roomViewModel.getSelectedMessage()?.eventId
                )
                MessageAction.QUICK_REACTION -> {
                    roomViewModel.selectedEmoji.value?.let { emoji ->
                        roomView.sendQuickReaction(
                            emoji,
                            roomViewModel.getSelectedMessage()?.eventId
                        )
                    }
                }
                MessageAction.REPLY -> {
                    showReplyView()

                }
                MessageAction.COPY -> {
                    doCopy()
                    roomViewModel.selectedMessage.value = null


                }
                MessageAction.FORWARD -> {
                    doForward()
                    roomViewModel.selectedMessage.value = null

                }
                MessageAction.DOWNLOAD -> {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            WRITE_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                            this,
                            READ_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        downloadMediaFile()
                    } else {
                        ActivityCompat.requestPermissions(
                            this, arrayOf(
                                WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE
                            ), 104
                        )
                    }

                }
                else -> {
                    val currentMessage = roomViewModel.getSelectedMessage()
                    currentMessage?.eventId?.let { it1 -> roomView.deleteMessage(it1) }
                    roomViewModel.selectedMessage.value = null

                }

            }
        }

        if (intent != null) {
            roomId = intent.getStringExtra(EXTRA_ROOM_ID) ?: ""

            if (roomId.isNotEmpty()) {
                if (intent.getBooleanExtra(EXTRA_SHOW_ROOM_INFO, false)) {
                    roomView.showRoomInfo()
                    intent.putExtra(EXTRA_SHOW_ROOM_INFO, false)
                }

                mApp?.currentForegroundRoom = roomId
            }

            // Send shared items into the room, if any.
            if (intent.action == Intent.ACTION_SEND) {

                if (intent.hasExtra(Intent.EXTRA_TEXT)) {
                    intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
                        roomView.sendMessage(it)
                    }
                }

                intent.uris.forEach {
                    if (intent.type?.isNotEmpty() == true)
                        sendAttachment(it, intent.type)
                }

                @Suppress("ControlFlowWithEmptyBody")
                if (intent.data != null) {
                    var mimeType = intent.type
                    if (mimeType == null)
                        mimeType = contentResolver.getType(intent.data!!)

                    sendAttachment(intent.data!!, mimeType)
                }
                else if (intent.hasExtra(Intent.EXTRA_STREAM)) {
                    val shareUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        intent.getParcelableExtra(Intent.EXTRA_STREAM, Uri::class.java)
                    }
                    else {
                        @Suppress("DEPRECATION")
                        intent.getParcelableExtra(Intent.EXTRA_STREAM) as? Uri
                    }

                    var mimeType = intent.type
                    if (shareUri != null) {
                        if (mimeType == null)
                            mimeType = contentResolver.getType(shareUri)

                        sendAttachment(shareUri, mimeType)
                    }
                }
                else if (intent.clipData != null) {
                    // TODO: What to do here?
                }

                // Remove attached data, to not send it again on rotate.
                intent.action = null
                intent.data = null
                intent.clipData = null
            }
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        applyStyleForToolbar()

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        roomView.setSelected(true)

        roomView.setupMentionAutoComplete(composeMessage)
        resendFailedMessages()

        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (backButtonHandler != null) {
                    backButtonHandler?.onClick(null)
                    return
                }

                finish()
            }
        })
    }

    private fun doForward() {

        val currentMessage = roomViewModel.getSelectedMessage()
        val messageText = currentMessage?.getTextEditableContent(true)

        val mc = currentMessage?.getLastMessageContent()

        var mimeType = ""

        when (mc) {
            is MessageWithAttachmentContent -> {
                mimeType = mc.mimeType.toString()
            }
        }

        if (mimeType.isNotEmpty()) {
            if (mc is MessageWithAttachmentContent) {
                mCoroutineScope.launch {
                    val fileMedia = mApp?.matrixSession?.fileService()?.downloadFile(
                        mc.getFileName(),
                        mimeType,
                        mc.getFileUrl(),
                        mc.encryptedFileInfo?.toElementToDecrypt()
                    )

                    if (fileMedia?.exists() == true) {
                        val contentUri = FileProvider.getUriForFile(
                            this@RoomActivity, "${this@RoomActivity.packageName}.provider",
                            fileMedia
                        )

                        startActivity(
                            ImApp.sImApp?.router?.router(
                                this@RoomActivity,
                                contentUri,
                                mimeType
                            )
                        )
                    }

                }

            }
        } else {
            (application as ImApp).router.router(this).apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, messageText)
                setTypeAndNormalize("text/plain")
                startActivity(this)
            }
        }
    }

    private fun doCopy() {
        val currentMessage = roomViewModel.getSelectedMessage()
        val messageText = currentMessage?.getTextEditableContent(true)
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager
        val clip = ClipData.newPlainText(getString(R.string.app_name), messageText)
        clipboard?.setPrimaryClip(clip)

        Toast.makeText(this, R.string.action_copied, Toast.LENGTH_SHORT).show()

    }

    private fun downloadMediaFile() {
        if (checkPermissions()) {
            val mimeType: String? =
                when (val mc = roomViewModel.getSelectedMessage()?.getLastMessageContent()) {
                    is MessageWithAttachmentContent -> {
                        mc.mimeType
                    }
                    else -> {
                        null
                    }
                }

            val attachment = roomViewModel.getSelectedMessageAttachment()
            if (mimeType != null && attachment != null) {
                val exportPath =
                    SecureMediaStore.exportPath(applicationContext, mimeType, attachment, true)
                try {
                    SecureMediaStore.exportContent(
                        mimeType,
                        attachment,
                        exportPath
                    )

                    Toast.makeText(
                        applicationContext,
                        "Downloaded: $exportPath",
                        Toast.LENGTH_LONG
                    ).show()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

    }


    private fun checkPermissions(): Boolean {
        val permissionCheck = ContextCompat.checkSelfPermission(
            this,
            WRITE_EXTERNAL_STORAGE
        )
        if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(
                this, arrayOf(WRITE_EXTERNAL_STORAGE),
                1
            )
            return false
        }
        return true
    }


    fun showReplyView() {
        mBinding.replyMessageLayout.messageInfoView.visibility = View.VISIBLE
        mBinding.replyMessageLayout.messageImageView.visibility = View.GONE
        val currentMessage = roomViewModel.getSelectedMessage()
        val senderName =
            if (currentMessage?.senderInfo?.displayName.isNullOrBlank()) currentMessage?.senderInfo?.userId
            else currentMessage?.senderInfo?.displayName
        mBinding.replyMessageLayout.root.visibility = View.VISIBLE
        mBinding.replyMessageLayout.messageSenderTextView.text = senderName
        var message = currentMessage?.getTextEditableContent(true)
        if (message?.startsWith(">") == true) {
            message = message.split("\n\n")[1]
        }
        mBinding.replyMessageLayout.messageContentTextView.text = message
        mBinding.replyMessageLayout.closeImageView.setOnClickListener {
            mBinding.replyMessageLayout.root.visibility = View.GONE
        }

        mBinding.replyMessageLayout.messageImageView.visibility = View.GONE

        /**
         * //this is all not working //TODO disable for now1
        val imagePath = roomViewModel.imageUri.value!!

        when (val messageContent = currentMessage?.getLastMessageContent()) {
        is MessageImageContent, is MessageStickerContent, is MessageVideoContent -> {
        mBinding.replyMessageLayout.messageImageView.visibility = View.VISIBLE
        GlideUtils.loadImageFromUri(
        this,
        imagePath,
        mBinding.replyMessageLayout.messageImageView
        )


        }
        is MessageFileContent, is MessageWithAttachmentContent -> {
        mBinding.replyMessageLayout.messageImageView.visibility = View.VISIBLE
        roomViewModel.imageUri.value = Uri.EMPTY
        val mimeType = getMimeType(messageContent.msgType, messageContent.body)
        setAttachmentImage(mimeType)
        }
        else -> {

        if (imagePath != Uri.EMPTY) {
        mBinding.replyMessageLayout.messageImageView.visibility = View.VISIBLE
        GlideUtils.loadImageFromUri(
        this,
        imagePath,
        mBinding.replyMessageLayout.messageImageView
        )

        } else {
        mBinding.replyMessageLayout.messageImageView.visibility = View.GONE
        }
        }

        }**/
    }

    @Suppress("unused")
    private fun setAttachmentImage(mimeType: String) {
        when {
            mimeType.isEmpty() -> {
                mBinding.replyMessageLayout.messageImageView.setImageResource(R.drawable.file_unknown)
            }

            mimeType.contains("html") -> {

                mBinding.replyMessageLayout.messageImageView.setImageResource(R.drawable.file_unknown)

            }

            mimeType == "text/csv" -> {

                mBinding.replyMessageLayout.messageImageView.setImageResource(R.drawable.proofmodebadge)
            }

            mimeType.contains("pdf") -> {
                mBinding.replyMessageLayout.messageImageView.setImageResource(R.drawable.file_pdf)
            }

            mimeType.contains("doc") || mimeType.contains("word") -> {
                mBinding.replyMessageLayout.messageImageView.setImageResource(R.drawable.file_doc)
            }

            mimeType.contains("zip") -> {
                mBinding.replyMessageLayout.messageImageView.setImageResource(R.drawable.file_zip)
            }

            else -> {
                mBinding.replyMessageLayout.messageImageView.setImageResource(R.drawable.file_unknown)
            }
        }

    }

    override fun applyStyleForToolbar() {
        supportActionBar?.title = roomView.title

        // Not set color.
        val preferenceProvider = PreferenceProvider(applicationContext)
        val themeColorHeader = preferenceProvider.getHeaderColor()
        var themeColorText = preferenceProvider.getTextColor()
        val themeColorBg = preferenceProvider.getBackgroundColor()

        if (themeColorHeader != -1) {
            if (themeColorText == -1) themeColorText =
                MainActivity.getContrastColor(themeColorHeader)

            window.navigationBarColor = themeColorHeader
            window.statusBarColor = themeColorHeader
            window.setTitleColor(themeColorText)

            toolbar.setBackgroundColor(themeColorHeader)
            toolbar.setTitleTextColor(themeColorText)
        }

        if (themeColorBg != -1) {
            mainContent.setBackgroundColor(themeColorBg)
            inputLayout.setBackgroundColor(themeColorBg)

            if (themeColorText != -1) {
                composeMessage.setTextColor(themeColorText)
                composeMessage.setHintTextColor(themeColorText)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        roomView.setSelected(true)
        mApp?.currentForegroundRoom = roomId
    }

    override fun onPause() {
        super.onPause()
        roomView.setSelected(false)
        mApp?.currentForegroundRoom = ""
    }

    override fun onDestroy() {
        super.onDestroy()
        roomView.setSelected(false)
        roomView.dispose()
        mApp?.currentForegroundRoom = ""
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        setIntent(intent)
        // Set last read date now!
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_conversation_detail_group, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (backButtonHandler != null) {
                    backButtonHandler?.onClick(null)
                    return true
                }

                finish()
                return true
            }

            R.id.menu_group_info -> {
                roomView.showRoomInfo()
                return true
            }

            R.id.menu_live_mode -> {
                startLiveMode()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ADD_MEDIA) {
            if (resultCode != RESULT_OK) return

            for (mediaUri in Matisse.obtainResult(data)) {
                sendAttachment(mediaUri)
            }
        } else {
            // Need to keep this for the Matisse library, which didn't get an overhaul in the
            // last 1.5 years. Grml.
            @Suppress("DEPRECATION")
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    fun startImagePicker() {
        if (ActivityCompat.checkSelfPermission(
                this,
                READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    READ_EXTERNAL_STORAGE
                )
            ) {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                        startImagePicker()
                    }
                    .show()
            } else {
                mStartImagePickerIfPermitted.launch(READ_EXTERNAL_STORAGE)
            }
        } else {
            Matisse.from(this)
                .choose(MimeType.ofAll())
                .countable(true)
                .maxSelectable(100)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(GlideEngine())
                .showPreview(false)
                .forResult(REQUEST_ADD_MEDIA)
        }
    }

    fun startPhotoTaker() {
        if (ActivityCompat.checkSelfPermission(
                this,
                CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA)) {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                        startPhotoTaker()
                    }
                    .show()
            } else {
                mStartPhotoTakerIfPermitted.launch(CAMERA)
            }
        } else {
            mTakePicture.launch(mApp?.router?.camera(this, oneAndDone = true))
        }
    }

    fun startFilePicker(mimeType: String = "*/*") {
        if (ActivityCompat.checkSelfPermission(
                this,
                READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    READ_EXTERNAL_STORAGE
                )
            ) {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                        startFilePicker(mimeType)
                    }
                    .show()
            } else {
                mStartFilePickerIfPermitted.launch(CAMERA)
            }
        } else {
            // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
            // browser.
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)

            // Filter to only show results that can be "opened", such as a
            // file (as opposed to a list of contacts or timezones)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            // Filter to show only images, using the image MIME data type.
            // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
            // To search for all documents available via installed storage providers,
            // it would be "*/*".
            intent.type = mimeType

            mSendFile.launch(Intent.createChooser(intent, getString(R.string.invite_share)))
        }
    }

    fun startStoryEditor() {
        mCreateStory.launch(mApp?.router?.storyEditor(this))
    }

    fun startAudioRecording() {
        if (ActivityCompat.checkSelfPermission(
                this,
                RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, RECORD_AUDIO)) {
                Snackbar.make(historyView, R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                        startAudioRecording()
                    }
                    .show()
            } else {
                mStartAudioRecordingIfPermitted.launch(RECORD_AUDIO)
            }
        } else {
            val am = getSystemService(AUDIO_SERVICE) as? AudioManager

            if (am?.mode == AudioManager.MODE_NORMAL) {
                val fileName = UUID.randomUUID().toString().substring(0, 8) + ".m4a"
                mAudioFilePath = File(filesDir, fileName)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    mMediaRecorder = MediaRecorder(this)
                }
                else {
                    @Suppress("DEPRECATION")
                    mMediaRecorder = MediaRecorder()
                }

                mMediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
                mMediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                mMediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

                // Maybe we can modify these in the future, or allow people to tweak them.
                mMediaRecorder?.setAudioChannels(1)
                mMediaRecorder?.setAudioEncodingBitRate(44100 * 16)
                mMediaRecorder?.setAudioSamplingRate(44100)
                mMediaRecorder?.setOutputFile(mAudioFilePath!!.absolutePath)

                try {
                    isAudioRecording = true
                    mMediaRecorder?.prepare()
                    mMediaRecorder?.start()
                } catch (e: Exception) {
                    Timber.e(e, "couldn't start audio")
                }
            }
        }
    }

    fun stopAudioRecording(send: Boolean) {
        if (mMediaRecorder != null && mAudioFilePath != null && isAudioRecording) {
            try {
                mMediaRecorder?.stop()
                mMediaRecorder?.reset()
                mMediaRecorder?.release()

                if (send) {
                    val uriAudio = Uri.fromFile(mAudioFilePath)
                    sendAttachment(uriAudio, "audio/x-m4a")
                } else {
                    mAudioFilePath?.delete()
                }
            } catch (ise: IllegalStateException) {
                Timber.w(ise, "error stopping audio recording")
            } catch (re: RuntimeException) {
                // Stop can fail so we should catch this here.

                Timber.w(re, "error stopping audio recording")
            }

            isAudioRecording = false
        }
    }

    fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null) {
        try {
            var mimeType = fallbackMimeType
            if (mimeType == null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(contentUri.toString())
                )
            }

            val attachment =
                AttachmentHelper.createFromContentUri(contentResolver, contentUri, mimeType)

            mApp?.matrixSession?.roomService()?.getRoom(roomId)?.sendService()?.sendMedia(
                attachment,
                true, setOf(roomId), null
            )
        } catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }

    fun sendShareRequest(request: ShareRequest) {
        sendAttachment(request.media, request.mimeType)
    }

    private fun startLiveMode() {
        startActivity(mApp?.router?.story(this, roomId, contributorMode = true))
    }

    private fun resendFailedMessages() {
        mApp?.matrixSession?.roomService()?.getRoom(roomId)?.sendService()?.resendAllFailedMessages()
    }

    private val mSendFile =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val uri = it.data?.data ?: return@registerForActivityResult

            sendAttachment(uri, it.data?.type)
        }

    private val mTakePicture =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val request = ShareRequest()
            request.deleteFile = false
            request.resizeImage = false
            request.importContent = false
            request.media = it.data?.data
            request.mimeType = it.data?.type

            if (request.mimeType == "image/jpeg") {
                try {
                    roomView.setMediaDraft(request)
                } catch (e: Exception) {
                    Timber.w(e, "error setting media draft")
                }
            } else {
                sendShareRequest(request)
            }
        }

    private val mCreateStory =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            val request = ShareRequest()
            request.deleteFile = false
            request.resizeImage = false
            request.importContent = false
            request.media = it.data?.data
            request.mimeType = it.data?.type

            sendShareRequest(request)
        }

    val requestImageView =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != RESULT_OK) return@registerForActivityResult

            if (it.data?.hasExtra("resendImageUri") == true) {
                val request = ShareRequest()
                request.deleteFile = false
                request.resizeImage = false
                request.importContent = false
                request.media = Uri.parse(it.data?.getStringExtra("resendImageUri"))
                request.mimeType = it.data?.getStringExtra("resendImageMimeType")

                sendShareRequest(request)
            }
        }

    private val mStartImagePickerIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startImagePicker()
        }

    private val mStartPhotoTakerIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startPhotoTaker()
        }

    private val mStartAudioRecordingIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startAudioRecording()
        }

    private val mStartFilePickerIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startFilePicker()
        }

    override fun onQuickResponseClick(timeline: TimelineEvent, attachment: Uri?, thumbnail: Uri?) {
        roomViewModel.selectedMessage.value = timeline
        roomViewModel.attachmentUri.value = attachment

        if (thumbnail == null) {
            roomViewModel.attachmentThumbnailUri.value = attachment
        } else {
            roomViewModel.attachmentThumbnailUri.value = thumbnail
        }

        supportFragmentManager.let {
            QuickResponseFragment.newInstance().apply {
                show(it, tag)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            104 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                downloadMediaFile()
            } else {
                // Permission Denied
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}
