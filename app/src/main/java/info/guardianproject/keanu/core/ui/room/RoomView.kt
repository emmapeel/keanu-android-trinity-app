@file:Suppress("unused")

package info.guardianproject.keanu.core.ui.room

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Message
import android.text.Editable
import android.text.InputType
import android.text.Spannable
import android.text.TextWatcher
import android.view.*
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.inputmethod.InputContentInfoCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.snackbar.Snackbar
import com.otaliastudios.autocomplete.Autocomplete
import com.otaliastudios.autocomplete.AutocompleteCallback
import com.otaliastudios.autocomplete.AutocompletePresenter
import com.otaliastudios.autocomplete.CharPolicy
import com.tougee.recorderview.AudioRecordView
import com.vanniktech.emoji.EmojiEditText
import com.vanniktech.emoji.EmojiPopup
import com.vanniktech.emoji.SingleEmojiTrait
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.type.CustomTypefaceEditText
import info.guardianproject.keanu.core.ui.RoundedAvatarDrawable
import info.guardianproject.keanu.core.ui.mention.PillImageSpan
import info.guardianproject.keanu.core.ui.mention.TextPillsUtils
import info.guardianproject.keanu.core.ui.mention.member.UserPresenter
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.WrapContentLinearLayoutManager
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.ui.legacy.SimpleAlertHandler
import info.guardianproject.keanuapp.ui.stickers.StickerManager
import info.guardianproject.keanuapp.ui.stickers.StickerPagerAdapter
import info.guardianproject.keanuapp.ui.widgets.ShareRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.getRoom
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.sync.SyncState
import org.matrix.android.sdk.api.util.MatrixItem
import org.matrix.android.sdk.api.util.toMatrixItem
import timber.log.Timber
import java.io.IOException
import kotlin.math.hypot

open class RoomView(protected var mActivity: RoomActivity, protected var mRoomId: String) {

    companion object {
        private const val VIEW_TYPE_CHAT = 1
        private const val SHOW_DATA_ERROR = 9997
        private const val SHOW_TYPING = 9996

        private class ChatViewHandler(activity: Activity?) : SimpleAlertHandler(activity) {

            override fun handleMessage(msg: Message) {
                when (msg.what) {
                    ImApp.EVENT_CONNECTION_DISCONNECTED -> {
                        Timber.d("Handle event connection disconnected.")
                        promptDisconnectedEvent(msg)
                        return
                    }
                    SHOW_DATA_ERROR -> {
                        msg.data.getString("file")
                        val error = msg.data.getString("err")
                        Toast.makeText(
                            mActivity,
                            "Error transferring file: $error",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    SHOW_TYPING -> {
                        val isTyping = msg.data.getBoolean("typing")

                        (mActivity as? RoomActivity)?.typingView?.visibility =
                            if (isTyping) View.VISIBLE else View.GONE
                    }
                }

                super.handleMessage(msg)
            }
        }
    }

    private val mApp
        get() = mActivity.application as? ImApp

    private val mSession
        get() = mApp?.matrixSession

    private val mRoom: Room?

    private val mHandler: SimpleAlertHandler

    private var mShareDraft: ShareRequest? = null

    private var mViewDeleteVoice: View? = null

    private var mIsSelected = false

    private var mMessageAdapter: MessageListAdapter? = null

    private var mRemoteNickname: String

    var icon: RoundedAvatarDrawable? = null

    var accountId: Long = -1

    var type = VIEW_TYPE_CHAT
        private set

    private val mUpdateChatCallback = Runnable { updateChat() }

    private var mIsListening = false

    private var mStickerPager: ViewPager? = null

    private val mStickerBox: RelativeLayout
        get() = mActivity.stickerBox

    private var emojiPopup: EmojiPopup? = null

    private var mSnackbar: Snackbar? = null
    private var itemTouchHelper: ItemTouchHelper? = null

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mActivity.historyView))
    }

    init {
        mHandler = ChatViewHandler(mActivity)
        mRoom = mSession?.getRoom(mRoomId)
        mRemoteNickname = mRoom?.roomSummary()?.displayNameWorkaround ?: ""

        bindQuery()
        initViews()

        mActivity.composeMessage.isEnabled = true
        mActivity.btnSend.isEnabled = true

        if (mSession?.syncService()?.hasAlreadySynced() == false) {
            mSession?.syncService()?.getSyncStateLive()?.observe(mActivity) { newStatus ->
                when (newStatus) {
                    is SyncState.Running -> {
                        if (mSnackbar == null) {
                            mSnackbar =
                                Snackbar.make(
                                    mActivity.historyView,
                                    mActivity.getString(R.string.loading),
                                    Snackbar.LENGTH_LONG
                                )
                            mSnackbar?.show()
                        }
                    }

                    else -> {
                        mSnackbar?.dismiss()
                        mSnackbar = null
                    }
                }
            }
        }

    }


    fun setSelected(isSelected: Boolean) {

        if (mIsSelected != isSelected) {
            //state changed

            mIsSelected = isSelected

            if (mIsSelected) {
                startListening()

                mActivity.composeMessage.requestFocus()

                userActionDetected()
                updateGroupTitle()

                mApp?.cancelNotification(mRoomId)

                if (!hasJoined()) showJoinGroupUI()
            } else {
                stopListening()
                sendTypingStatus(false)
            }
        }
    }

    fun dispose() {
        mMessageAdapter?.dispose()
    }

    @SuppressLint("ClickableViewAccessibility")
    protected fun initViews(): Boolean {

        val llm = WrapContentLinearLayoutManager(mActivity.historyView.context)

        //      llm.stackFromEnd = true
        mActivity.historyView.layoutManager = llm
        mActivity.historyView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                when (llm.findFirstVisibleItemPosition()) {
                    0 -> {
                        //top of list
                        mMessageAdapter?.pageBack()
                    }
                }

                if (mActivity.historyView.canScrollVertically(1)) {
                    mActivity.btJumpToBottom.show()
                } else {
                    mActivity.btJumpToBottom.hide()
                }
            }
        })

        mActivity.btJumpToBottom.setOnClickListener {
            jumpToBottom()
        }

        mActivity.composeMessage.setReachContentClickListener(object :
            CustomTypefaceEditText.OnReachContentSelect {
            override fun onReachContentClick(inputContentInfoCompat: InputContentInfoCompat) {
                val request = ShareRequest()
                request.deleteFile = false
                request.resizeImage = false
                request.importContent = true
                request.media = inputContentInfoCompat.contentUri
                request.mimeType = "image/gif"

                mActivity.sendShareRequest(request)
            }
        })

        mViewDeleteVoice = mActivity.findViewById(R.id.viewDeleteVoice)

        mActivity.btnDeleteVoice.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_MOVE) {
                mActivity.btnDeleteVoice.setBackgroundColor(
                    ResourcesCompat.getColor(
                        mActivity.btnDeleteVoice.resources, android.R.color.holo_red_light, null
                    )
                )
            }

            false
        }

        mActivity.btnAttach.setOnClickListener { toggleAttachMenu() }

        mActivity.mediaPreviewCancel.setOnClickListener { clearMediaDraft() }

        mActivity.btnAttachPicture.setOnClickListener {
            mActivity.attachPanel.visibility = View.GONE
            mActivity.startImagePicker()
        }

        mActivity.btnTakePicture.setOnClickListener {
            mActivity.attachPanel.visibility = View.GONE
            mActivity.startPhotoTaker()
        }

        mActivity.btnAttachAudio.setOnClickListener {
            mActivity.attachPanel.visibility = View.GONE
            mActivity.startFilePicker("audio/*")
        }

        mActivity.btnAttachFile.setOnClickListener {
            mActivity.attachPanel.visibility = View.GONE
            mActivity.startFilePicker("*/*")
        }

        mActivity.btnAttachSticker.setOnClickListener { toggleStickers() }

        mActivity.btnCreateStory.setOnClickListener {
            mActivity.attachPanel.visibility = View.GONE
            mActivity.startStoryEditor()
        }

        mActivity.audioRecordView.activity = mActivity
        mActivity.audioRecordView.callback = object : AudioRecordView.Callback {

            override fun onRecordStart(audio: Boolean) {
                Timber.d("onRecordStart: $audio")
                mActivity.startAudioRecording()
            }

            override fun onRecordEnd() {
                Timber.d("onRecordEnd")
                if (mActivity.isAudioRecording) {
                    mActivity.stopAudioRecording(true)
                }
            }

            override fun isReady(): Boolean {
                return true
            }

            override fun onRecordCancel() {
                Timber.d("onRecordCancel")
                if (mActivity.isAudioRecording) {
                    mActivity.stopAudioRecording(false)
                }
            }
        }

        mActivity.composeMessage.setOnTouchListener { _, _ ->
            sendTypingStatus(mActivity.composeMessage.text?.isNotEmpty() ?: false)
            false
        }

        mActivity.composeMessage.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            sendTypingStatus(hasFocus && (mActivity.composeMessage.text?.isNotEmpty() ?: false))
        }

        mActivity.composeMessage.setOnKeyListener(object : View.OnKeyListener {

            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER -> {
                            sendMessage()
                            return true
                        }
                        KeyEvent.KEYCODE_ENTER -> {
                            if (event.isAltPressed) {
                                mActivity.composeMessage.append("\n")
                                return true
                            }
                        }
                    }
                }

                return false
            }
        })

        mActivity.composeMessage.setOnEditorActionListener(object : OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent): Boolean {
                if (event.isAltPressed) {
                    return false
                }

                val imm =
                    mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                if (imm?.isActive(v) == true) {
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }

                sendMessage()

                return true
            }
        })

        // TODO: this is a hack to implement BUG #1611278, when dispatchKeyEvent() works with
        //  the soft keyboard, we should remove this hack.
        mActivity.composeMessage.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence, start: Int, before: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {
                userActionDetected()
            }
        })

        mActivity.btnSend.setOnClickListener { onSendButtonClicked() }

        mMessageAdapter = createRecyclerViewAdapter()
        mActivity.historyView.adapter = mMessageAdapter
        val messageSwipeController = MessageSwipeController(
            context = mActivity.baseContext,
            object : SwipeControllerActions {
                override fun showReplyUI(timeLine: TimelineEvent) {
                    mActivity.roomViewModel.selectedMessage.value = timeLine
                    mActivity.showReplyView()


                }
            })

        itemTouchHelper = ItemTouchHelper(messageSwipeController)
        itemTouchHelper?.attachToRecyclerView(mActivity.historyView)
        return true
    }

    open fun isAtBottom(): Boolean? {
        return mActivity.historyView.canScrollVertically(1)
    }

    open fun setupMentionAutoComplete(editTextSendMessage: EditText) {
        if (mRoom == null) {
            return
        }
        val elevation = 6f
        val backgroundDrawable: Drawable = ColorDrawable(Color.WHITE)
        val presenter: AutocompletePresenter<RoomMemberSummary> =
            UserPresenter(mActivity.baseContext, mRoom)
        val callback: AutocompleteCallback<RoomMemberSummary> =
            object : AutocompleteCallback<RoomMemberSummary> {
                override fun onPopupItemClicked(
                    editable: Editable,
                    item: RoomMemberSummary
                ): Boolean {
                    // editable.clear()
                    insertMatrixItem(
                        mActivity.composeMessage,
                        editable,
                        "@",
                        item.toMatrixItem()
                    )
                    return true
                }

                override fun onPopupVisibilityChanged(shown: Boolean) {}
            }

        Autocomplete.on<RoomMemberSummary>(editTextSendMessage)
            .with(elevation)
            .with(backgroundDrawable)
            .with(CharPolicy('@'))
            .with(presenter)
            .with(callback)
            .build()
    }

    @Suppress("SameParameterValue")
    private fun insertMatrixItem(
        editText: EditText,
        editable: Editable,
        firstChar: String,
        matrixItem: MatrixItem
    ) {
        // Detect last firstChar and remove it
        var startIndex = editable.lastIndexOf(firstChar)
        if (startIndex == -1) {
            startIndex = 0
        }

        // Detect next word separator
        var endIndex = editable.indexOf(" ", startIndex)
        if (endIndex == -1) {
            endIndex = editable.length
        }

        // Replace the word by its completion
        val displayName = matrixItem.displayName ?: ""

        // Adding trailing space " " or ": " if the user started mention someone
        val displayNameSuffix =
            if (firstChar == "@" && startIndex == 0) {
                ": "
            } else {
                " "
            }

        editable.replace(startIndex, endIndex, "$displayName$displayNameSuffix")

        // Add the span
        val span = PillImageSpan(
            context = editText.context,
            matrixItem = matrixItem
        )
        span.bind(editText)

        editable.setSpan(
            span,
            startIndex,
            startIndex + displayName.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

    protected open fun onSendButtonClicked() {
        if (mActivity.composeMessage.visibility == View.VISIBLE) {
            if (mActivity.roomViewModel.getSelectedMessage() != null) {
                sendReply(mActivity.roomViewModel.getSelectedMessage()!!.eventId)
                mActivity.roomViewModel.selectedMessage.value = null
                mActivity.mBinding.replyMessageLayout.root.visibility = View.GONE

            } else {
                sendMessage()
            }
        } else {
            mActivity.btnSend.setImageResource(R.drawable.ic_send_secure)
            mActivity.btnSend.visibility = View.GONE

            mActivity.composeMessage.visibility = View.VISIBLE

            mActivity.audioRecordView.visibility = View.VISIBLE
        }
    }

    protected open fun createRecyclerViewAdapter(): MessageListAdapter? {
        return MessageListAdapter(mActivity, this, mRoomId, mActivity)
    }

    private fun sendTypingStatus(isTyping: Boolean) {
        if (isTyping) {
            mRoom?.typingService()?.userIsTyping()
        } else {
            mRoom?.typingService()?.userStopsTyping()
        }
    }

    private fun startListening() {
        mIsListening = true

        mMessageAdapter?.startListening()
    }

    private fun stopListening() {
        mMessageAdapter?.stopListening()

        mIsListening = false
    }

    private fun updateChat() {
        updateGroupTitle()
    }

    private fun hasJoined(): Boolean {
        val userId = mSession?.myUserId ?: return false

        // TODO
        return mRoom?.membershipService()?.getRoomMember(userId)?.membership?.name.equals(
            Membership.JOIN.name,
            ignoreCase = true
        )
    }

    private fun showJoinGroupUI() {
        val joinGroupView = mActivity.joinGroupView
        joinGroupView.visibility = View.VISIBLE

        val btnJoinAccept = mActivity.btnJoinAccept
        val btnJoinDecline = mActivity.btnJoinDecline

        val title = mActivity.roomJoinTitle
        title.text = title.context.getString(R.string.room_invited)

        btnJoinAccept.setOnClickListener {
            mCoroutineScope.launch {
                try {
                    mRoom?.roomId?.let { it1 -> mSession?.roomService()?.joinRoom(it1,"") }
                } catch (failure: Throwable) {
                    Timber.d(failure)

                    return@launch mActivity.finish()
                }

                initViews()
            }

            joinGroupView.visibility = View.GONE
        }

        btnJoinDecline.setOnClickListener {
            mCoroutineScope.launch {
                mRoom?.roomId?.let { it1 -> mSession?.roomService()?.leaveRoom(it1,"") }
            }

            joinGroupView.visibility = View.GONE

            mActivity.finish()
        }
    }

    val title: String
        get() = if (mRemoteNickname.isNotBlank()) mRemoteNickname else mRoomId

    private fun updateGroupTitle() {
        mRemoteNickname = mRoom?.roomSummary()?.displayNameWorkaround ?: ""

        // Update title
        mActivity.supportActionBar?.title = mRemoteNickname
    }

    private fun bindQuery(): Boolean {
        mHandler.post(mUpdateChatCallback)
        return true
    }

    private fun jumpToBottom() {
        jumpToPosition(mMessageAdapter?.itemCount ?: 0)

        mActivity.btJumpToBottom.hide()
    }

    fun jumpToPosition(position: Int) {
        val count = mMessageAdapter?.itemCount ?: 0

        if (count > 0) {
            if (position < count) {
                mActivity.btJumpToBottom.show()
            } else {
                mActivity.btJumpToBottom.hide()
            }

            if (position > 0) {
                mActivity.historyView.layoutManager?.scrollToPosition(position - 1)
            } else {
                mActivity.historyView.layoutManager?.scrollToPosition(0)
            }
        }
    }

    fun closeChatSession() {
        mCoroutineScope.launch {
            mRoom?.roomId?.let { it1 -> mSession?.roomService()?.leaveRoom(it1,"") }
        }
    }

    fun showRoomInfo() {
        mActivity.startActivity(mApp?.router?.groupDisplay(mActivity, mRoomId))
    }

    protected open fun sendMessage() {
        if (mShareDraft != null) {
            Timber.v("sendMessage")

            mActivity.sendShareRequest(mShareDraft!!)
            mShareDraft = null

            mActivity.mediaPreviewContainer.visibility = View.GONE
        }

        val msg = mActivity.composeMessage.text.toString()
        val replyId = (mMessageAdapter?.lastSelectedView as? MessageListItem)?.eventId
        val userMap = getMentionedUserInMessage(msg)
        val formattedBody = TextPillsUtils.processSpecialSpansToHtml(msg, userMap)
        if (msg == formattedBody) {
            if (msg.isNotEmpty()) sendMessage(formattedBody, replyId)
        } else {
            if (msg.isNotEmpty()) sendFormattedMessage(msg, formattedBody, replyId)
        }
    }

    protected open fun sendReply(eventId: String) {
        if (mShareDraft != null) {
            Timber.v("sendReply")

            mActivity.sendShareRequest(mShareDraft!!)
            mShareDraft = null

            mActivity.mediaPreviewContainer.visibility = View.GONE
        }

        val msg = mActivity.composeMessage.text.toString()
        if (msg.isNotEmpty()) sendMessage(msg, eventId)
    }

    @Throws(IOException::class)
    fun setMediaDraft(mediaDraft: ShareRequest) {
        mShareDraft = mediaDraft

        mActivity.mediaPreviewContainer.visibility = View.VISIBLE

        val bmpPreview = SecureMediaStore.getThumbnailFile(mActivity, mediaDraft.media, 1000)
        mActivity.mediaPreview.setImageBitmap(bmpPreview)

        mActivity.attachPanel.visibility = View.GONE

        mActivity.composeMessage.setText(" ")

        toggleInputMode()
    }

    private fun clearMediaDraft() {
        mShareDraft = null

        mActivity.composeMessage.setText("")

        mActivity.mediaPreviewContainer.visibility = View.GONE
    }

    fun deleteMessage(eventId: String) {

        if (eventId.contains("local")) {

            mRoom?.timelineService()?.getTimelineEvent(eventId)?.let {
                mRoom.sendService().deleteFailedEcho(it)
            }
        } else {
            mRoom?.timelineService()?.getTimelineEvent(eventId)?.root?.let {
                mRoom.sendService().redactEvent(it, "")

            }
        }
    }

    fun resendMessage(resendMsg: String?, mimeType: String?) {
        if (resendMsg.isNullOrEmpty()) return

        if (SecureMediaStore.isVfsUri(resendMsg) || SecureMediaStore.isContentUri(resendMsg)) {
            // What do we do with this?
            val request = ShareRequest()
            request.deleteFile = false
            request.resizeImage = false
            request.importContent = false
            request.media = Uri.parse(resendMsg)
            request.mimeType = mimeType

            try {
                mActivity.sendShareRequest(request)
            } catch (e: Exception) {
                Timber.w(e, "error setting media draft")
            }
        } else {
            sendMessage(resendMsg, null)
        }
    }

    private fun sendMessage(msg: String, replyId: String?) {
        sendMessage(msg, replyId, false)
        mActivity.composeMessage.setText("")
        mActivity.composeMessage.requestFocus()
        sendTypingStatus(false)
        jumpToBottom()
    }

    private fun sendFormattedMessage(
        msg: String,
        formattedMessage: String,
        targetEventId: String? = null
    ) {
        if (targetEventId?.isNotEmpty() == true) {
            val event = mRoom?.timelineService()?.getTimelineEvent(targetEventId)
            if (event != null) {
                mRoom?.relationService()?.replyToMessage(event, msg, autoMarkdown = true)
            }
        } else {
            //mRoom?.sendFormattedTextMessage(msg, formattedMessage, MessageType.MSGTYPE_TEXT)
            mRoom?.sendService()?.sendFormattedTextMessage(msg, formattedMessage, MessageType.MSGTYPE_TEXT)
        }
        mActivity.composeMessage.setText("")
        mActivity.composeMessage.requestFocus()
        sendTypingStatus(false)
        jumpToBottom()
    }

    fun sendMessage(
        msg: String,
        targetEventId: String? = null,
        isQuickReaction: Boolean = false
    ): Boolean {

        // Don't send empty messages.
        if (msg.isBlank()) return false

        if (isQuickReaction && targetEventId?.isNotEmpty() == true) {
            mRoom?.relationService()?.sendReaction(targetEventId, msg)

            mMessageAdapter?.notifyItemChanged(targetEventId)

        } else if (targetEventId?.isNotEmpty() == true) {
            val event = mRoom?.timelineService()?.getTimelineEvent(targetEventId)

            if (event != null) {
                mRoom?.relationService()?.replyToMessage(event, msg, autoMarkdown = true)
            }
        } else {
            mRoom?.sendService()?.sendTextMessage(msg, MessageType.MSGTYPE_TEXT, true)
        }

        jumpToBottom()

        return true
    }

    private fun userActionDetected() {
        sendTypingStatus(mActivity.composeMessage.text?.isNotEmpty() == true)
        toggleInputMode()
    }

    private fun toggleInputMode() {
        if (mActivity.composeMessage.text?.isNotEmpty() == true && mActivity.btnSend.visibility == View.GONE) {
            mActivity.audioRecordView.visibility = View.GONE
            mActivity.btnAttachSticker.visibility = View.GONE
            mActivity.btnSend.visibility = View.VISIBLE
            mActivity.btnSend.setImageResource(R.drawable.ic_send_secure)
        } else if (mActivity.composeMessage.text.isNullOrEmpty()) {
            mActivity.btnAttachSticker.visibility = View.VISIBLE
            mActivity.audioRecordView.visibility = View.VISIBLE
            mActivity.btnSend.visibility = View.GONE
        }
    }

    fun sendQuickReaction(reaction: String, messageId: String?) {
        sendMessage(reaction, messageId, true)
    }

    private fun toggleAttachMenu() {
        if (mActivity.attachPanel.visibility == View.GONE) {

            // Get the center for the clipping circle.
            val cx = mActivity.attachPanel.left
            val cy = mActivity.attachPanel.height

            // Get the final radius for the clipping circle.
            val finalRadius = hypot(cx.toDouble(), cy.toDouble()).toFloat()

            // Create the animator for this view (the start radius is zero).
            val anim = ViewAnimationUtils.createCircularReveal(
                mActivity.attachPanel, cx, cy, 0f, finalRadius
            )

            // Make the view visible and start the animation.
            mActivity.attachPanel.visibility = View.VISIBLE

            anim.start()

            // Check if no view has focus:
            val view = mActivity.currentFocus
            if (view != null) {
                (mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
                    ?.hideSoftInputFromWindow(view.windowToken, 0)
            }
        } else {
            // Get the center for the clipping circle.
            val cx = mActivity.attachPanel.left
            val cy = mActivity.attachPanel.height

            // Get the initial radius for the clipping circle.
            val initialRadius = hypot(cx.toDouble(), cy.toDouble()).toFloat()

            // Create the animation (the final radius is zero).
            val anim = ViewAnimationUtils.createCircularReveal(
                mActivity.attachPanel, cx, cy, initialRadius, 0f
            )

            // Make the view invisible when the animation is done.
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    mActivity.attachPanel.visibility = View.GONE
                }
            })

            // Start the animation.
            anim.start()

        }

        mStickerBox.visibility = View.GONE
    }

    private fun toggleStickers() {
        if (mStickerPager == null) initStickers()

        mStickerBox.visibility =
            if (mStickerBox.visibility == View.GONE) View.VISIBLE else View.GONE
    }

    @Synchronized
    private fun initStickers() {
        mStickerPager = mActivity.stickerPager

        val emojiGroups = StickerManager.getInstance(mActivity).emojiGroups

        if (emojiGroups.isNotEmpty()) {
            val emojiPagerAdapter = StickerPagerAdapter(
                mActivity, ArrayList(emojiGroups)
            ) { s ->
                sendStickerCode(s.assetUri)

                toggleStickers()
            }

            mStickerPager?.adapter = emojiPagerAdapter
        }
    }

    /**
     * Generate a :pack-sticker: code
     */
    private fun sendStickerCode(assetUri: Uri) {
// TODO: This is not finished, yet.
//
//        val content = StickerHelper.createStickerContent(mActivity, assetUri)
//
//        mRoom?.sendEvent(EventType.STICKER, content.toContent())
//
// We would need to do the same as `DefaultSendService.sendMedia`, including
// esp. `DefaultSendService.internalSendMedia` which is all hidden an non-overridable. D'oh.
// Better let a patch to MatrixSDK get upstreamed?

        val stickerCode = StringBuffer()
        stickerCode.append(':')
        stickerCode.append(assetUri.pathSegments[assetUri.pathSegments.size - 2])
        stickerCode.append('-')
        stickerCode.append(assetUri.lastPathSegment?.split("\\.".toRegex())?.firstOrNull())
        stickerCode.append(':')

        sendMessage(stickerCode.toString(), null)
    }

    fun showQuickReactionsPopup(messageId: String?) {
        if (emojiPopup != null) return

        val rootView = mActivity.historyView.parent as View

        try {
            val editText = EmojiEditText(mActivity)
            editText.imeOptions = EditorInfo.IME_ACTION_SEND
            editText.inputType = InputType.TYPE_NULL
            editText.setBackgroundColor(-0x80000000)
            editText.setOnClickListener { v -> // If tap on background, close popup!
                if (emojiPopup != null) {
                    val imm =
                        v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                    imm?.hideSoftInputFromWindow(v.windowToken, 0)
                    emojiPopup?.dismiss()
                }
            }

            SingleEmojiTrait.install(editText)

            (rootView as ViewGroup).addView(editText, ViewGroup.LayoutParams.MATCH_PARENT, 1)

            rootView.postDelayed({
                emojiPopup = EmojiPopup.Builder.fromRootView(rootView)
                    .setOnEmojiPopupDismissListener {
                        emojiPopup = null
                        rootView.removeView(editText)
                    }
                    .setOnEmojiClickListener { _, variant ->
                        sendQuickReaction(variant.unicode, messageId)
                        emojiPopup?.dismiss()
                    }
                    .build(editText)

                emojiPopup?.toggle()
            }, 200)
        } catch (e: Exception) {
            Timber.d(e)
        }
    }

    private fun getMentionedUserInMessage(message: String): Map<String?, RoomMemberSummary> {
        val builder = RoomMemberQueryParams.Builder()
        builder.memberships = arrayListOf(Membership.JOIN)
        builder.excludeSelf = true
        val query = builder.build()
        val userMap = mRoom?.membershipService()?.getRoomMembers(query)
            ?.associateBy({ it.displayName }, { it })
        return if (userMap.isNullOrEmpty()) {
            emptyMap()
        } else {
            message.split(" ")
                .filter { content -> userMap.containsKey(content.replace(":", "")) }
                .map { content ->
                    userMap[content.replace(":", "")]!!
                }
                .associateBy({ it.displayName }, { it })
        }
    }

    fun forwardSelectedMedia() {
        mMessageAdapter?.forwardMediaFile()
    }
}
