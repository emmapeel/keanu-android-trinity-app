package info.guardianproject.keanu.core.ui.widgets

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.verification.VerificationManager
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.VerificationRequestItemBinding
import org.matrix.android.sdk.api.session.crypto.verification.VerificationState
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.model.ReferencesAggregatedContent
import org.matrix.android.sdk.api.session.room.model.message.MessageVerificationRequestContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent

class VerificationRequestItemHolder(private var mBinding: VerificationRequestItemBinding): RecyclerView.ViewHolder(mBinding.root),
        View.OnClickListener {

    init {
        mBinding.btAccept.setOnClickListener(this)
        mBinding.btDecline.setOnClickListener(this)
    }

    private val mSession
        get() = ImApp.sImApp?.matrixSession

    private val mVerificationService
        get() = mSession?.cryptoService()?.verificationService()

    private val mContext
        get() = mBinding.root.context

    private var mState = VerificationState.REQUEST

    private var mOtherUserId: String? = null

    private var mRoomId: String? = null

    fun bind(te: TimelineEvent) {
        te.annotations?.referencesAggregatedSummary?.content?.toModel<ReferencesAggregatedContent>()?.verificationState?.let {
            mState = it
        }

        mBinding.tvTitle.text = mContext.getString(R.string._wants_to_verify, te.senderInfo.disambiguatedDisplayName)
        mBinding.tvMatrixId.text = te.senderInfo.userId

        when (mState) {
            VerificationState.REQUEST -> {
                mBinding.btAccept.visibility = View.VISIBLE
                mBinding.btDecline.visibility = View.VISIBLE
                mBinding.tvStatus.visibility = View.GONE

                val content = te.root.getClearContent().toModel<MessageVerificationRequestContent>()
                if (content?.toUserId != mSession?.myUserId) return showStatus(mContext.getString(R.string.Cancelled))

                mOtherUserId = te.senderInfo.userId
                mRoomId = te.roomId
            }

            VerificationState.CANCELED_BY_ME,
            VerificationState.CANCELED_BY_OTHER -> {
                showStatus(mContext.getString(R.string.Cancelled))
            }

            VerificationState.DONE -> {
                showStatus(mContext.getString(R.string.Verification_Successful_))
            }

            VerificationState.WAITING -> {
                showStatus(mContext.getString(R.string.Waiting_on_challenge_negotiation_))
            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            mBinding.btAccept -> {
                if (mOtherUserId != null && mRoomId != null) {
                    VerificationManager.instance.interactiveVerifyFromEvent(mOtherUserId!!, mRoomId!!)
                }
                else {
                    showStatus(mContext.getString(R.string.Error))
                }
            }

            mBinding.btDecline -> {
                if (mOtherUserId != null && mRoomId != null) {
                    VerificationManager.instance.getRequestByUserIdAndRoom(mOtherUserId!!, mRoomId!!)?.let {
                        mVerificationService?.cancelVerificationRequest(it)
                    }
                }

                showStatus(mContext.getString(R.string.Cancelled))
            }
        }
    }

    private fun showStatus(text: String) {
        mBinding.btAccept.visibility = View.GONE
        mBinding.btDecline.visibility = View.GONE
        mBinding.tvStatus.visibility = View.VISIBLE

        mBinding.tvStatus.text = text
    }
}