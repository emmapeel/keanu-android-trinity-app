package info.guardianproject.keanu.core.ui.widgets

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.TransferListener
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.SecureMediaStore
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityVideoViewerBinding
import org.apache.commons.io.IOUtils
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class VideoViewActivity : AppCompatActivity() {

    private var mShowResend = false

    private var mMediaUri: Uri? = null

    private var mMimeType: String? = null

    private lateinit var mExoPlayer: ExoPlayer

    private lateinit var mBinding: ActivityVideoViewerBinding
    private var mApp: ImApp? = null

    @SuppressLint("ClickableViewAccessibility")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityVideoViewerBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mApp = application as ImApp
        mMediaUri = intent.data
        mMimeType = intent.type
        mShowResend = intent.getBooleanExtra("showResend", false)

        supportActionBar?.show()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        title = mMediaUri?.lastPathSegment

        mExoPlayer = ExoPlayer.Builder(this).build()
        mBinding.root.player = mExoPlayer
    }

    override fun onResume() {
        super.onResume()

        val item = mMediaUri?.let { MediaItem.fromUri(it) }
        if (item != null) mExoPlayer.setMediaItem(item)

        mExoPlayer.prepare()
    }

    override fun onPause() {
        super.onPause()

        mExoPlayer.stop()
        mExoPlayer.release()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_message_context, menu)

        menu.findItem(R.id.menu_message_copy).isVisible = false
        menu.findItem(R.id.menu_message_resend).isVisible = mShowResend
        menu.findItem(R.id.menu_message_delete).isVisible = false

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }

            R.id.menu_message_forward -> {
                forwardMediaFile()
                return true
            }

            R.id.menu_message_share -> {
                exportMediaFile()
                return true
            }

            R.id.menu_message_resend -> {
                resendMediaFile()
                return true
            }

            R.id.menu_downLoad -> {
                if (ContextCompat.checkSelfPermission(
                        this@VideoViewActivity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    val sd = File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                            .toString() + "/Keanu/"
                    )
                    val extension =
                        mMediaUri!!.path!!.substring(mMediaUri!!.path!!.lastIndexOf("."))
                    val filename = "Keanu_$mDateTime$extension"
                    DownloadVideo().execute(mMediaUri, filename, sd)
                } else {
                    ActivityCompat.requestPermissions(
                        this@VideoViewActivity,
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        104
                    )
                }
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private inner class DownloadVideo : AsyncTask<Any?, Void?, Long>() {
        var storagePath: String? = null

        override fun doInBackground(vararg params: Any?): Long {
            val videoUri = params[0] as Uri
            val filename = params[1] as String
            val sd = params[2] as File

            storagePath = sd.path

            var bytesCopied: Long = 0

            val videoPath = videoUri.path ?: return bytesCopied

            if (!sd.exists()) {
                sd.mkdirs()
            }

            val dest = File(sd, filename)

            try {
                val fis = FileInputStream(File(videoPath))
                val fos = FileOutputStream(dest, false)

                fis.use {
                    fos.use {
                        bytesCopied = IOUtils.copyLarge(fis, fos)
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return bytesCopied
        }

        override fun onPostExecute(result: Long) {
            if (result > 0) {
                Toast.makeText(
                    applicationContext,
                    "Video Downloaded at :-$storagePath",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private val mDateTime: String
        get() = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US).format(Date())

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )

            return false
        }

        return true
    }

    private fun exportMediaFile() {
        val mimeType = mMimeType ?: return
        val mediaUri = mMediaUri ?: return

        if (checkPermissions()) {
            val exportPath = SecureMediaStore.exportPath(this, mimeType, mediaUri, true)
            val uri = FileProvider.getUriForFile(this, "${this.packageName}.provider", exportPath)
            startActivity(mApp?.router?.router(this, uri, mMimeType))
        }
    }

    private fun forwardMediaFile() {
        startActivity(mApp?.router?.router(this, mMediaUri, mMimeType))
    }

    private fun resendMediaFile() {
        val intentResult = Intent()
        intentResult.putExtra("resendImageUri", mMediaUri)
        intentResult.putExtra("resendImageMimeType", mMimeType)
        setResult(RESULT_OK, intentResult)
        finish()
    }

    class InputStreamDataSource(private val context: Context, private val dataSpec: DataSpec) :
        DataSource {
        private var inputStream: InputStream? = null
        private var bytesRemaining: Long = 0
        private var opened = false
        override fun addTransferListener(transferListener: TransferListener) {}

        @Throws(IOException::class)
        override fun open(dataSpec: DataSpec): Long {
            try {
                inputStream = convertUriToInputStream(context, dataSpec.uri)
                if (inputStream != null) {
                    val skipped = inputStream!!.skip(dataSpec.position)
                    if (skipped < dataSpec.position) throw EOFException()
                    if (dataSpec.length != C.LENGTH_UNSET.toLong()) {
                        bytesRemaining = dataSpec.length
                    } else {
                        bytesRemaining = inputStream!!.available().toLong()
                        if (bytesRemaining == Int.MAX_VALUE.toLong()) bytesRemaining =
                            C.LENGTH_UNSET.toLong()
                    }
                } else {
                    opened = false
                    return -1
                }
            } catch (e: IOException) {
                throw IOException(e)
            }
            opened = true
            return bytesRemaining
        }

        @Throws(IOException::class)
        override fun read(buffer: ByteArray, offset: Int, readLength: Int): Int {
            if (readLength == 0) {
                return 0
            } else if (bytesRemaining == 0L) {
                return C.RESULT_END_OF_INPUT
            }
            val bytesRead = try {
                val bytesToRead = if (bytesRemaining == C.LENGTH_UNSET.toLong()) {
                    readLength
                } else {
                    bytesRemaining.coerceAtMost(readLength.toLong()).toInt()
                }

                inputStream!!.read(buffer, offset, bytesToRead)
            } catch (e: IOException) {
                throw IOException(e)
            }
            if (bytesRead == -1) {
                if (bytesRemaining != C.LENGTH_UNSET.toLong()) {
                    throw IOException(EOFException())
                }
                return C.RESULT_END_OF_INPUT
            }
            if (bytesRemaining != C.LENGTH_UNSET.toLong()) {
                bytesRemaining -= bytesRead.toLong()
            }
            return bytesRead
        }

        override fun getUri(): Uri {
            return dataSpec.uri
        }

        override fun getResponseHeaders(): MutableMap<String, MutableList<String>> {
            return mutableMapOf()
        }

        @Throws(IOException::class)
        override fun close() {
            try {
                inputStream?.close()
            } finally {
                inputStream = null
                opened = false
            }
        }

        private fun convertUriToInputStream(context: Context, mediaUri: Uri): InputStream? {
            //Your implementation of obtaining InputStream from mediaUri
            return if (mediaUri.scheme == null || mediaUri.scheme == "vfs") {
                try {
                    FileInputStream(mediaUri.path)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    null
                }
            } else {
                try {
                    context.contentResolver.openInputStream(mediaUri)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    null
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            104 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val sd = File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                        .toString() + "/Keanu/"
                )
                val extension = mMediaUri!!.path!!.substring(mMediaUri!!.path!!.lastIndexOf("."))
                val filename = "Keanu_$mDateTime$extension"
                DownloadVideo().execute(mMediaUri, filename, sd)
            } else {
                // Permission Denied
                Toast.makeText(this@VideoViewActivity, "Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}