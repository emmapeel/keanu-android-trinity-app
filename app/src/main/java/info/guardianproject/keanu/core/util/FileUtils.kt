package info.guardianproject.keanu.core.util

import android.content.Context
import timber.log.Timber
import java.io.File

object FileUtils {
    private val TAG = this.javaClass.name ?: "FileUtils"
    const val DIR_PICTURES = "Pictures"

    private fun getCustomExternalCacheDir(context: Context, childDirName: String): File? {
        try {
            val externalPictureDirPath: String = context.externalCacheDir?.absolutePath + "/" + childDirName
            val externalPictureDir = File(externalPictureDirPath)
            externalPictureDir.mkdirs()
            return externalPictureDir
        } catch (exception: SecurityException) {
            Timber.e(TAG, "getCustomExternalCacheDir exception: " + exception)
        }
        return null
    }

    fun getExternalCachePictureDirectory(context: Context): File? {
        return getCustomExternalCacheDir(context, DIR_PICTURES)
    }
}