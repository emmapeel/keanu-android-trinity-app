/**
 *
 */
package info.guardianproject.keanu.core.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.os.Environment
import androidx.exifinterface.media.ExifInterface
import org.apache.commons.io.IOUtils
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.io.UnsupportedEncodingException
import java.nio.channels.FileChannel
import java.util.UUID

/**
 * Copyright (C) 2014 Guardian Project.  All rights reserved.
 *
 * @author liorsaar
 */
object SecureMediaStore {
    private const val DEFAULT_IMAGE_WIDTH = 1080
    private const val LOG_TAG = "SecureMediaStore"
    private const val ReservedChars = "[|\\?*<\":>+/']"

    @JvmStatic
    fun list(parent: String?) {
        if (parent == null) return
        val file = File(parent)
        val list = file.listFiles() ?: return
        Timber.d("Dir=" + file.isDirectory + ";" + file.absolutePath + ";files=" + list.size)
        for (fileChild in list) {
            if (fileChild.isDirectory) {
                list(fileChild.absolutePath)
            } else {
                Timber.d(fileChild.absolutePath + " (" + fileChild.length() / 1000 + "KB)")
            }
        }
    }

    @JvmStatic
    @Throws(IOException::class)
    fun deleteLargerThan(fileLengthFilter: Long) {
        val file = File("/")
        // if the session doesn't have any ul/dl files - bail
        if (!file.exists()) {
            return
        }
        // delete recursive
        deleteBySize(file, fileLengthFilter)
    }

    @JvmStatic
    @Throws(IOException::class)
    fun deleteSession(sessionId: String) {
        val dirName = "/$sessionId"
        val file = File(dirName)
        // if the session doesnt have any ul/dl files - bail
        if (!file.exists()) {
            return
        }
        // delete recursive
        delete(dirName)
    }

    @JvmStatic
    @Throws(IOException::class)
    private fun deleteBySize(parent: File, sizeFilter: Long) {
        // if a file or an empty directory - delete it
        if (parent.length() > sizeFilter) {
            if (!parent.delete()) {
                throw IOException("Error deleting $parent")
            }
            return
        }

        // directory - recurse
        val list = parent.listFiles() ?: return
        for (fileToDelete in list) {
            deleteBySize(fileToDelete, sizeFilter)
        }
    }

    @JvmStatic
    @Throws(IOException::class)
    private fun delete(parentName: String) {
        val parent = File(parentName)
        // if a file or an empty directory - delete it
        if (!parent.isDirectory || (parent.list()?.size ?: 0) == 0) {
            //    Log.e(TAG, "delete:" + parent );
            if (!parent.delete()) {
                throw IOException("Error deleting $parent")
            }
            return
        }
        // directory - recurse
        val list = parent.list() ?: arrayOf()
        for (i in list.indices) {
            val childName = parentName + "/" + list[i]
            delete(childName)
        }
        delete(parentName)
    }

    private const val VFS_SCHEME = "vfs"
    private const val CONTENT_SCHEME = "content"
    private const val ENCODING = "UTF-8"

    @JvmStatic
    @Throws(UnsupportedEncodingException::class)
    fun vfsUri(filename: String): Uri {
        return Uri.parse(VFS_SCHEME + ":" + filename)
    }

    @JvmStatic
    fun isVfsUri(uri: Uri): Boolean {
        return VFS_SCHEME == uri.scheme
    }

    @JvmStatic
    fun isContentUri(uri: Uri): Boolean {
        return CONTENT_SCHEME == uri.scheme
    }

    @JvmStatic
    fun isContentUri(uriString: String): Boolean {
        return if (uriString.isEmpty()) false else uriString.startsWith(CONTENT_SCHEME + ":/")
    }

    fun isVfsUri(uriString: String): Boolean {
        return if (uriString.isEmpty()) false else uriString.startsWith(VFS_SCHEME + ":/")
    }

    @JvmStatic
    fun getThumbnailVfs(uri: Uri, thumbnailSize: Int): Bitmap? {
        val path = uri.path ?: return null
        val image = File(path)
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        options.inInputShareable = true
        options.inPurgeable = true
        try {
            val fis = FileInputStream(File(image.path))
            BitmapFactory.decodeStream(fis, null, options)
        } catch (e: Exception) {
            LogCleaner.warn(
                LOG_TAG,
                "unable to read vfs thumbnail$e"
            )
            return null
        }
        if (options.outWidth == -1 || options.outHeight == -1) return null
        val originalSize =
            if (options.outHeight > options.outWidth) options.outHeight else options.outWidth
        val opts = BitmapFactory.Options()
        opts.inSampleSize = originalSize / thumbnailSize
        return try {
            val fis = FileInputStream(File(image.path))
            BitmapFactory.decodeStream(fis, null, opts)
        } catch (e: FileNotFoundException) {
            LogCleaner.warn(
                LOG_TAG,
                "can't find IOcipher file: " + image.path
            )
            null
        } catch (oe: OutOfMemoryError) {
            LogCleaner.error(
                LOG_TAG,
                "out of memory loading thumbnail: " + image.path,
                oe
            )
            null
        }
    }

    @JvmStatic
    @Throws(IOException::class)
    fun importContent(context: Context, sessionId: String, sourceFile: File): Uri {
        val targetPath = sessionId + "-" + UUID.randomUUID() + "-" + sourceFile.name
        val fileMedia = File(context.cacheDir, targetPath)
        copyToVfs(sourceFile, fileMedia)
        return Uri.fromFile(fileMedia)
    }

    /**
     * Copy device content into vfs.
     * All imported content is stored under /SESSION_NAME/
     * The original full path is retained to facilitate browsing
     * The session content can be deleted when the session is over
     *
     * @param sessionId
     * @return vfs uri
     * @throws IOException
     */
    @JvmStatic
    @Throws(IOException::class)
    fun importContent(
        context: Context,
        sessionId: String,
        fileName: String,
        sourceStream: InputStream?
    ): Uri {
        val targetPath = sessionId + "-" + UUID.randomUUID() + "-" + fileName
        val fileMedia = File(context.cacheDir, targetPath)
        copyToVfs(sourceStream, fileMedia)

        return Uri.fromFile(fileMedia)
    }

    /**
     * Copy device content into vfs.
     * All imported content is stored under /SESSION_NAME/
     * The original full path is retained to facilitate browsing
     * The session content can be deleted when the session is over
     * @param sessionId
     * @return vfs uri
     * @throws IOException
     */
    @JvmStatic
    @Throws(IOException::class)
    fun createContentPath(sessionId: String, fileName: String): Uri {
        //list("/");
        var targetPath =
            "/" + sessionId + "/upload/" + UUID.randomUUID().toString() + '/' + fileName
        targetPath = createUniqueFilename(targetPath)
        mkdirs(targetPath)

        //list("/");
        return vfsUri(targetPath)
    }

    /**
     * Resize an image to an efficient size for sending via OTRDATA, then copy
     * that resized version into vfs. All imported content is stored under
     * /SESSION_NAME/ The original full path is retained to facilitate browsing
     * The session content can be deleted when the session is over
     *
     * @param sessionId
     * @return vfs uri
     * @throws IOException
     */
    @JvmStatic
    @JvmOverloads
    @Throws(IOException::class)
    fun resizeAndImportImage(
        context: Context,
        sessionId: String,
        uri: Uri,
        mimeType: String?,
        maxImageSize: Int = DEFAULT_IMAGE_WIDTH
    ): Uri {
        var targetPath = sessionId + "-" + UUID.randomUUID()
        var savePNG = false
        val hasPngExtension = uri.path?.endsWith(".png") ?: false
        val hasGifExtension = uri.path?.endsWith(".gif") ?: false

        if (hasPngExtension
             || mimeType != null && mimeType.contains("png")
             || hasGifExtension
             || mimeType != null && mimeType.contains("gif")) {
            savePNG = true
            targetPath += ".png"
        } else {
            targetPath += ".jpg"
        }

        //load lower-res bitmap
        val bmp = getThumbnailFile(context, uri, maxImageSize)
        val baos = ByteArrayOutputStream()

        bmp?.let {
            if (savePNG) bmp.compress(Bitmap.CompressFormat.PNG, 100, baos)
            else bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos)
        }

        val fileMedia = File(context.cacheDir, targetPath)
        val out = FileOutputStream(fileMedia)
        IOUtils.write(baos.toByteArray(), out)
        out.close()
        bmp?.recycle()
        return Uri.fromFile(fileMedia)
    }

    @JvmStatic
    @Throws(FileNotFoundException::class)
    fun openInputStream(context: Context, uri: Uri): InputStream? {
        return if (uri.scheme != null && uri.scheme == "vfs")
            FileInputStream(uri.path)
        else context.contentResolver.openInputStream(
            uri
        )
    }

    @JvmStatic
    @Throws(IOException::class)
    fun getThumbnailFile(context: Context, uri: Uri, thumbnailSize: Int): Bitmap? {

        //Decode bitmap size
        var fileInputStream: InputStream = openInputStream(context, uri) ?: return null
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeStream(fileInputStream, null, options)
        if (options.outWidth == -1 || options.outHeight == -1) return null
        if (options.outHeight > options.outWidth) options.outHeight else options.outWidth
        fileInputStream.close()

        //Get a scaled down version of the bitmap
        fileInputStream = openInputStream(context, uri) ?: return null
        val opts = BitmapFactory.Options()
        opts.inSampleSize = calculateInSampleSize(options, thumbnailSize, thumbnailSize)
        var scaledBitmap = BitmapFactory.decodeStream(fileInputStream, null, opts)
        fileInputStream.close()
        fileInputStream = openInputStream(context, uri) ?: return scaledBitmap

        //Rotate bitmap if it's rotated
        var orientationD = 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val exif = ExifInterface(fileInputStream)
            when (exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)) {
                ExifInterface.ORIENTATION_ROTATE_90 -> orientationD = 90
                ExifInterface.ORIENTATION_ROTATE_180 -> orientationD = 180
                ExifInterface.ORIENTATION_ROTATE_270 -> orientationD = 270
            }
        }

        fileInputStream.close()
        if (orientationD != 0) scaledBitmap = rotateBitmap(scaledBitmap, orientationD)
        return scaledBitmap
    }

    @JvmStatic
    @Throws(IOException::class)
    fun exportContent(mimeType: String?, mediaUri: Uri, exportPath: File?) {
        val sourcePath = mediaUri.path
        copyToExternal(sourcePath, exportPath)
    }

    @JvmStatic
    fun exportPath(context: Context, mimeType: String, mediaUri: Uri, isExternal: Boolean): File {
        val targetFileName = mediaUri.lastPathSegment ?: (System.currentTimeMillis().toString() + ".jpg")
        val targetFile = if (isExternal) {
            if (mimeType.startsWith("image")) {
                val externalPictureCacheDir = FileUtils.getExternalCachePictureDirectory(context)
                File(externalPictureCacheDir, targetFileName)
            } else if (mimeType.startsWith("audio")) {
                File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC),
                    targetFileName
                )
            } else {
                File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    targetFileName
                )
            }
        } else {
            File(context.cacheDir, targetFileName)
        }
        return createUniqueFilenameExternal(targetFile)
    }

    @JvmStatic
    @Throws(IOException::class)
    fun copyToVfs(sourceFile: File?, fileOut: File?) {
        val fis: InputStream = FileInputStream(sourceFile)
        val fos = FileOutputStream(fileOut, false)
        IOUtils.copy(fis, fos)
        fos.close()
    }

    @JvmStatic
    @Throws(IOException::class)
    fun copyToVfs(sourceIS: InputStream?, fileOut: File?) {

        // copy
        val fos: OutputStream = FileOutputStream(fileOut, false)
        IOUtils.copy(sourceIS, fos)
        fos.close()
    }

    @JvmStatic
    @Throws(IOException::class)
    fun copyToVfs(buf: ByteArray?, targetPath: String?) {
        if (targetPath == null) return
        val file = File(targetPath)
        val out = FileOutputStream(file)
        IOUtils.write(buf, out)
        out.close()
    }

    @JvmStatic
    @Throws(IOException::class)
    fun copyToExternal(sourcePath: String?, targetPath: File?) {
        if (sourcePath == null) return
        // copy
        var fis: FileInputStream? = null
        var fos: FileOutputStream? = null
        try {
            fis = FileInputStream(File(sourcePath))
            fos = FileOutputStream(targetPath, false)
            IOUtils.copy(fis, fos)
        } catch (e: FileNotFoundException) {
            Timber.e(e)
        } finally {
            fos?.close()
            fis?.close()
        }
    }

    @JvmStatic
    @Throws(IOException::class)
    private fun mkdirs(targetPath: String) {
        val targetFile = File(targetPath)
        if (!targetFile.exists()) {
            val dirFile = targetFile.parentFile
            if (dirFile != null && !dirFile.exists()) {
                val created = dirFile.mkdirs()
                if (!created) {
                    throw IOException("Error creating $targetPath")
                }
            }
        }
    }

    @JvmStatic
    fun exists(path: String?): Boolean {
        if (path == null) return false
        return File(path).exists()
    }

    @JvmStatic
    fun sessionExists(sessionId: String): Boolean {
        return exists("/$sessionId")
    }

    @JvmStatic
    private fun createUniqueFilename(filename: String): String {
        if (!exists(filename)) {
            return filename
        }
        var count = 1
        var uniqueName: String
        var file: File
        do {
            uniqueName = formatUnique(filename, count++)
            file = File(uniqueName)
        } while (file.exists())
        return uniqueName
    }

    @JvmStatic
    private fun formatUnique(filename: String, counter: Int): String {
        val lastDot = filename.lastIndexOf(".")
        return if (lastDot != -1) {
            val name = filename.substring(0, lastDot)
            val ext = filename.substring(lastDot)
            "$name-$counter$ext"
        } else {
            filename + counter
        }
    }

    @JvmStatic
    fun checkDownloadExists(sessionId: String, filenameFromUrl: String): File {
        val filename =
            "/" + sessionId + "/download/" + filenameFromUrl.replace(ReservedChars.toRegex(), "_")
        return File(filename)
    }

    @JvmStatic
    fun getDownloadFilename(
        sessionId: String,
        filenameFromUrl: String
    ): String {
        val filename = "/$sessionId/download/" + filenameFromUrl.replace(
            ReservedChars.toRegex(),
            "_"
        )
        return createUniqueFilename(filename)
    }

    private fun createUniqueFilenameExternal(filename: File): File {
        if (!filename.exists()) {
            return filename
        }
        var count = 1
        var uniqueName: String
        var file: File
        do {
            uniqueName = formatUnique(filename.name, count++)
            file = File(filename.parentFile, uniqueName)
        } while (file.exists())
        return file
    }

    @JvmStatic
    fun getImageOrientation(imagePath: String?): Int {
        var rotate = 0
        try {
            val exif = ExifInterface(imagePath!!)
            when (exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return rotate
    }

    @JvmStatic
    fun rotateBitmap(
        bitmap: Bitmap?,
        rotate: Int
    ): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(rotate.toFloat())
        return Bitmap.createBitmap(bitmap!!, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    @JvmStatic
    fun calculateInSampleSize(
        options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int
    ): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            val halfHeight = height / 2
            val halfWidth = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize >= reqHeight
                && halfWidth / inSampleSize >= reqWidth
            ) {
                inSampleSize *= 2
            }
        }
        return inSampleSize
    }

    @Throws(IOException::class)
    private fun copyFile(src: File, dst: File) {
        var inChannel: FileChannel? = null
        var outChannel: FileChannel? = null
        try {
            inChannel = FileInputStream(src).channel
            outChannel = FileOutputStream(dst).channel
        } catch (e: FileNotFoundException) {
            Timber.e(e)
        }
        try {
            inChannel?.transferTo(0, inChannel.size(), outChannel)
        } finally {
            inChannel?.close()
            outChannel?.close()
        }
    }
}