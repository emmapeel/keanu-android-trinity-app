package info.guardianproject.keanu.core.util.extensions

import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Parcelable

fun Intent.copy(oldIntent: Intent): Intent {
    action = oldIntent.action
    type = oldIntent.type
    data = oldIntent.data
    clipData = oldIntent.clipData
    oldIntent.extras?.let { putExtras(it) }

    return this
}

val Intent.uris: List<Uri>
    get() {
        val uris = ArrayList<Uri>()

        data?.let { uris.add(it) }

        clipData?.let {
            repeat(it.itemCount) { i ->
                uris.add(it.getItemAt(i).uri)
            }
        }

        return uris
    }

fun <T : Parcelable> Intent.getParcelableExtraCompat(name: String, clazz: Class<T>): T? {
    return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
        @Suppress("DEPRECATION")
        getParcelableExtra(name)
    } else {
        getParcelableExtra(name, clazz)
    }
}
