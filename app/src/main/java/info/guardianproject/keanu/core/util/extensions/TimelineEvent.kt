package info.guardianproject.keanu.core.util.extensions

import android.content.Context
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.model.message.*
import org.matrix.android.sdk.api.session.room.model.relation.ReactionContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent

fun TimelineEvent.getSummary(context: Context): String {
    val name = senderInfo.disambiguatedDisplayName
    val mc = getLastMessageContent()

    when {
        mc is MessageImageContent -> return context.getString(R.string._sent_an_image, name)

        mc is MessageAudioContent -> return context.getString(R.string._sent_audio, name)

        mc is MessageVideoContent -> return context.getString(R.string._sent_a_video, name)

        mc is MessageStickerContent -> return context.getString(R.string._sent_a_sticker_, name, mc.body)

        mc is MessageWithAttachmentContent -> return context.getString(R.string._sent_a_file, name)

        mc?.body?.isNotEmpty() == true -> return "$name: ${mc.body}"

        mc == null -> {
            val reaction = root.getClearContent().toModel<ReactionContent>()

            if (reaction != null) {

                return context.getString(
                    R.string._reacted_with_, name,
                    reaction.relatesTo?.key ?: context.getString(R.string.unknown))
            }
        }
    }

    return context.getString(R.string._did_something, name)
}

fun TimelineEvent.isReaction(): Boolean {
    return root.getClearType() == EventType.REACTION
}

fun TimelineEvent.getRelatedEventId(): String? {
    return root.getClearContent()?.toModel<MessageRelationContent>()?.relatesTo?.eventId
}

fun TimelineEvent.getLastMessageBody(): String? {
    return getLastMessageContent()?.body
}