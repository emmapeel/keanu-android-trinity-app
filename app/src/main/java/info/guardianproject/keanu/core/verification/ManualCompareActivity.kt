package info.guardianproject.keanu.core.verification

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityManualCompareBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import org.matrix.android.sdk.api.extensions.getFingerprintHumanReadable
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.crypto.model.CryptoDeviceInfo

class ManualCompareActivity: BaseActivity(), View.OnClickListener {

    private lateinit var mBinding: ActivityManualCompareBinding

    private val mSession: Session?
        get() = (application as? ImApp)?.matrixSession

    private var mDevice: CryptoDeviceInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val userId = intent.getStringExtra(VerificationManager.EXTRA_USER_ID)
        val deviceId = intent.getStringExtra(VerificationManager.EXTRA_DEVICE_ID)

        userId?.let {
            mDevice = mSession?.cryptoService()?.getCryptoDeviceInfo(userId, deviceId)
        }

        mBinding = ActivityManualCompareBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.btKeysMatch.setOnClickListener(this)
        mBinding.btInteractive.setOnClickListener(this)

        mBinding.tvDeviceName.text = getString(R.string.Name_, mDevice?.displayName() ?: "–")

        mBinding.tvDeviceId.text = getString(R.string.Identifier_, deviceId ?: "–")

        mBinding.tvDeviceFingerprint.text = mDevice?.getFingerprintHumanReadable()

        supportActionBar?.title = getString(R.string.Verify_Device)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        val device = mDevice ?: return finish()

        if (v == mBinding.btKeysMatch) {
            VerificationManager.instance.verify(device, true)

            finish()
        }
        else {
            VerificationManager.instance.interactiveVerify(this, device) {
                finish()
            }
        }
    }
}