package info.guardianproject.keanuapp.ui;

import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import info.guardianproject.keanu.core.Preferences;
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider;
import info.guardianproject.keanuapp.R;


/**
 * Created by n8fr8 on 5/7/16.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceProvider preferenceProvider = new PreferenceProvider(getApplicationContext());

        if (preferenceProvider.getScreenshotInfo())
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE);

        //not set color

        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        int themeColorHeader = settings.getInt(Preferences.THEME_COLOR_HEADER,-1);
        int themeColorBg = settings.getInt(Preferences.THEME_COLOR_BG,-1);

         if (themeColorHeader != -1) {
             getWindow().setNavigationBarColor(themeColorHeader);
             getWindow().setStatusBarColor(themeColorHeader);

             if (getSupportActionBar() != null) {
                 getSupportActionBar().setBackgroundDrawable(new ColorDrawable(themeColorHeader));
             }
         }


        if (themeColorBg != -1)
        {
            getWindow().getDecorView().setBackgroundColor(themeColorBg);
        }
    }

    public void applyStyleForToolbar() {

        final PreferenceProvider preferenceProvider = new PreferenceProvider(getApplicationContext());
        int themeColorHeader = preferenceProvider.getHeaderColor();
        int themeColorText = preferenceProvider.getTextColor();

        Toolbar toolbar = findViewById(R.id.toolbar);

        if (themeColorHeader != -1) {
            toolbar.setBackgroundColor(themeColorHeader);
            toolbar.setTitleTextColor(themeColorText);
        }

    }

}
