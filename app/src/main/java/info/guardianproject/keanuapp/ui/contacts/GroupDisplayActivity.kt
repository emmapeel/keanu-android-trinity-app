package info.guardianproject.keanuapp.ui.contacts

import agency.tango.android.avatarview.AvatarPlaceholder
import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.Moshi
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanu.core.model.impl.BaseAddress
import info.guardianproject.keanu.core.ui.friends.FriendsPickerActivity
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager
import info.guardianproject.keanu.core.ui.room.RoomActivity
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.ImageChooserHelper
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.*
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.qr.QrShareAsyncTask
import info.guardianproject.keanuapp.ui.widgets.GroupAvatar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver.ThumbnailMethod
import org.matrix.android.sdk.api.session.events.model.Event
import org.matrix.android.sdk.api.session.events.model.EventType.STATE_ROOM_POWER_LEVELS
import org.matrix.android.sdk.api.session.getRoom
import org.matrix.android.sdk.api.session.getRoomSummary
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.*
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.session.room.powerlevels.PowerLevelsHelper
import org.matrix.android.sdk.api.session.room.powerlevels.Role
import org.matrix.android.sdk.api.session.room.state.isPublic
import org.matrix.android.sdk.api.util.Optional
import timber.log.Timber
import java.io.File
import java.net.URLEncoder

class GroupDisplayActivity : BaseActivity() {

    companion object {
        private const val MY_PERMISSIONS_REQUEST_CAMERA = 1
    }

    inner class GroupMember(
        var username: String? = null,
        nickname: String? = null,
        var role: Role = Role.Default,
        var affiliation: String? = null,
        var avatarUrl: String? = null,
        var online: Boolean = false
    ) {
        private var _nickname: String? = nickname

        var nickname: String?
            get() = if (_nickname.isNullOrBlank()) username else _nickname
            set(value) {
                _nickname = value
            }
    }

    private val mApp: ImApp?
        get() = application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private lateinit var mRoomId: String

    private var mYou = GroupMember()

    private var mMembers = ArrayList<GroupMember>()

    private var mHandler: Handler = Handler(Looper.getMainLooper())
    private var mRoomSummary: RoomSummary? = null
    private var mRoom: Room? = null

    private var mPlHelper: PowerLevelsHelper? = null

    private var mActionAddFriends: View? = null
    private var mActionShare: View? = null
    private var headerViewHolder: HeaderViewHolder? = null

    private lateinit var mBinding: RecyclerViewBinding
    private lateinit var preferenceProvider: PreferenceProvider

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }

    private val mImageChooserHelper by lazy {
        ImageChooserHelper(this)
    }

    private lateinit var mPickAvatar: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("in #onCreate!")
        preferenceProvider = PreferenceProvider(this.applicationContext)

        mBinding = RecyclerViewBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mRoomId = intent.getStringExtra(RoomActivity.EXTRA_ROOM_ID) ?: ""

        mRoom = mSession?.getRoom(mRoomId)
        mRoomSummary = mSession?.getRoomSummary(mRoomId)

        val query = RoomSummaryQueryParams.Builder()
        query.roomId = QueryStringValue.Equals(mRoomId, QueryStringValue.Case.SENSITIVE)

        mSession?.roomService()?.getRoomSummariesLive(query.build())?.observe(this) {
            it.firstOrNull()?.let { summary ->
                mRoomSummary = summary
                updateSession()
            }
        }

        supportActionBar?.title = ""
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mPickAvatar = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            mImageChooserHelper.getCallback(this::setAvatar)
        )
    }

    private fun initData() {
        mYou.username = mSession?.myUserId
        mYou.affiliation = "none"
        mYou.role = Role.Default
        mYou.avatarUrl = null

        updateMembers()
    }

    private fun initRecyclerView() {
        mBinding.root.adapter = object : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

            private val VIEW_TYPE_MEMBER = 0
            private val VIEW_TYPE_HEADER = 1
            private val VIEW_TYPE_FOOTER = 2

            private var mOriginalTextColor: Int? = null

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): RecyclerView.ViewHolder {
                val inflater = LayoutInflater.from(parent.context)

                return when (viewType) {
                    VIEW_TYPE_HEADER -> HeaderViewHolder(
                        AwesomeActivityGroupHeaderBinding.inflate(
                            inflater,
                            parent,
                            false
                        )
                    )
                    VIEW_TYPE_FOOTER -> FooterViewHolder(
                        AwesomeActivityGroupFooterBinding.inflate(
                            inflater,
                            parent,
                            false
                        )
                    )
                    else -> MemberViewHolder(
                        GroupMemberViewBinding.inflate(
                            inflater,
                            parent,
                            false
                        )
                    )
                }
            }

            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                if (holder is HeaderViewHolder) {
                    headerViewHolder = holder

                    checkNotificationState()

                    var avatarUrl: String? = mRoomSummary?.avatarUrl

                    if (avatarUrl?.isNotEmpty() == true) {
                        avatarUrl = mSession?.contentUrlResolver()
                            ?.resolveThumbnail(avatarUrl, 512, 512, ThumbnailMethod.SCALE)
                        GlideUtils.loadImageFromUri(
                            mApp?.applicationContext,
                            Uri.parse(avatarUrl),
                            headerViewHolder?.avatar,
                            false
                        )
                    } else {
                        val avatar = GroupAvatar(mRoomSummary?.displayNameWorkaround, false)
                        headerViewHolder?.avatar?.setImageDrawable(avatar)
                    }
                    headerViewHolder?.deleteFailedMessage?.setOnClickListener {
                        headerViewHolder?.clearProgressBar?.visibility = View.VISIBLE
                        Handler(Looper.getMainLooper()).postDelayed({
                            deleteFailedMessages()
                        }, 1500)

                    }

                    headerViewHolder?.avatar?.setOnClickListener { startAvatarTaker() }

                    headerViewHolder?.qr?.setOnClickListener {
                        try {
                            mCoroutineScope.launch {
                                mRoom?.aliasService()?.getRoomAliases()?.firstOrNull().let {
                                    if (it.isNullOrBlank()) return@let

                                    lifecycleScope.launch {
                                        @Suppress("BlockingMethodInNonBlockingContext")
                                        val inviteString = OnboardingManager.generateInviteLink(
                                            URLEncoder.encode(
                                                it,
                                                "UTF-8"
                                            )
                                        )
                                        OnboardingManager.inviteScan(
                                            this@GroupDisplayActivity,
                                            inviteString
                                        )
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    headerViewHolder?.tvRoomName?.text = mRoomSummary?.displayNameWorkaround
                    headerViewHolder?.tvRoomTopic?.text = mRoomSummary?.topic
                    mActionShare = headerViewHolder?.actionShare

                    headerViewHolder?.actionShare?.setOnClickListener {
                        try {
                            mCoroutineScope.launch {
                                mRoom?.aliasService()?.getRoomAliases()?.firstOrNull().let {
                                    if (it.isNullOrBlank()) {
                                        createAlias()
                                    } else {
                                        showShareSheet(it)
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            Timber.e(e, "couldn't generate QR code")
                        }
                    }

                    mActionAddFriends = headerViewHolder?.actionAddFriends
                    showAddFriends()

                    headerViewHolder?.actionNotifications?.setOnClickListener {
                        headerViewHolder?.checkNotifications?.isChecked =
                            !(headerViewHolder?.checkNotifications?.isChecked ?: false)
                    }

                    headerViewHolder?.checkNotifications?.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
                        setNotificationsEnabled(isChecked)
                    }

                    headerViewHolder?.checkNotifications?.isEnabled = true

                    if (Preferences.doGroupEncryption() && canInviteOthers(mYou)) {
                        headerViewHolder?.checkGroupEncryption?.isChecked = isGroupEncryptionEnabled

                        if (isGroupEncryptionEnabled) {
                            headerViewHolder?.checkGroupEncryption?.visibility = View.VISIBLE
                            headerViewHolder?.actionGroupEncryption?.visibility = View.VISIBLE
                            headerViewHolder?.checkGroupEncryption?.isEnabled = false
                        } else {
                            headerViewHolder?.checkGroupEncryption?.setOnCheckedChangeListener { _: CompoundButton?, isChecked: Boolean ->
                                isGroupEncryptionEnabled = isChecked
                            }
                            headerViewHolder?.actionGroupEncryption?.visibility = View.VISIBLE
                            headerViewHolder?.checkGroupEncryption?.visibility = View.VISIBLE
                            headerViewHolder?.checkGroupEncryption?.isEnabled = true
                        }
                    } else {
                        headerViewHolder?.actionGroupEncryption?.visibility = View.GONE
                    }

                    if (!canChangeName(mYou)) {
                        headerViewHolder?.ivEditRoomName?.visibility = View.GONE
                        headerViewHolder?.ivEditRoomTopic?.visibility = View.GONE
                    } else {
                        headerViewHolder?.ivEditRoomName?.setOnClickListener {
                            showEditAlert(
                                R.string.Change_room_name,
                                headerViewHolder?.tvRoomName?.text,
                                ::changeRoomName
                            )
                        }
                        headerViewHolder?.ivEditRoomName?.visibility = View.VISIBLE
                        headerViewHolder?.ivEditRoomName?.isEnabled = true

                        headerViewHolder?.ivEditRoomTopic?.setOnClickListener {
                            showEditAlert(
                                R.string.Change_room_topic,
                                headerViewHolder?.tvRoomTopic?.text,
                                ::changeRoomTopic
                            )
                        }
                        headerViewHolder?.ivEditRoomTopic?.visibility = View.VISIBLE
                        headerViewHolder?.ivEditRoomTopic?.isEnabled = true
                    }
                } else if (holder is FooterViewHolder) {
                    // Tint the "leave" text and drawable(s)
                    val colorAccent =
                        ResourcesCompat.getColor(resources, R.color.holo_orange_light, theme)

                    for (d in holder.actionLeave.compoundDrawables) {
                        d?.let { DrawableCompat.setTint(d, colorAccent) }
                    }

                    holder.actionLeave.setTextColor(colorAccent)
                    holder.actionLeave.setOnClickListener { confirmLeaveGroup() }
                } else if (holder is MemberViewHolder) {
                    if (mMembers.isEmpty()) {
                        holder.line1.setText(R.string.loading)
                        holder.line2.text = ""
                    } else {
                        // Reset the padding to match other views in this hierarchy.
                        val padding = resources.getDimensionPixelOffset(R.dimen.detail_view_padding)
                        holder.itemView.setPadding(
                            padding,
                            holder.itemView.paddingTop,
                            padding,
                            holder.itemView.paddingBottom
                        )

                        val idxMember = position - 1
                        val member = mMembers[idxMember]
                        var nickname = member.nickname

                        if (member.username == mYou.username) {
                            nickname += " " + getString(R.string.group_you)
                        }

                        holder.line1.text = nickname
                        holder.line2.text = member.username

                        when {
                            canGrantAdmin(member) -> {
                                holder.avatarCrown.setImageResource(R.drawable.ic_crown)
                                holder.avatarCrown.visibility = View.VISIBLE
                            }
                            member.affiliation == Membership.INVITE.name -> {
                                holder.avatarCrown.setImageResource(R.drawable.ic_message_wait_grey)
                                holder.avatarCrown.visibility = View.VISIBLE
                            }
                            else -> {
                                holder.avatarCrown.visibility = View.GONE
                            }
                        }

                        if (mOriginalTextColor == null) mOriginalTextColor =
                            holder.line1.currentTextColor
                        holder.line1.setTextColor(if (member.role is Role.Default) Color.GRAY else mOriginalTextColor!!)

                        holder.avatar.visibility = View.VISIBLE

                        GlideUtils.loadAvatar(
                            this@GroupDisplayActivity,
                            member.avatarUrl,
                            AvatarPlaceholder(nickname),
                            holder.avatar
                        )

                        holder.itemView.setOnClickListener { showMemberInfo(member) }
                    }
                }
            }

            override fun getItemCount(): Int {
                return if (mMembers.isEmpty()) 3 else (2 + mMembers.size)
            }

            override fun getItemViewType(position: Int): Int {
                return when (position) {
                    0 -> VIEW_TYPE_HEADER
                    itemCount - 1 -> VIEW_TYPE_FOOTER
                    else -> VIEW_TYPE_MEMBER
                }
            }
        }

        mBinding.root.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private fun deleteFailedMessages() {
        mRoom?.sendService()?.cancelAllFailedMessages()

        headerViewHolder?.clearProgressBar?.visibility = View.GONE

    }


    private fun updateSession() {
        initData()
        initRecyclerView()
    }

    override fun onResume() {
        super.onResume()

        updateSession()
    }

    fun startAvatarTaker() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_DENIED
        ) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.CAMERA
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Snackbar.make(mBinding.root, R.string.grant_perms, Snackbar.LENGTH_LONG).show()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.CAMERA),
                    MY_PERMISSIONS_REQUEST_CAMERA
                )
            }
        } else {
            mPickAvatar.launch(mImageChooserHelper.intent)
        }
    }

    private fun updateMembers() {
        updatePowerLevels(mRoom?.stateService()?.getStateEvent(STATE_ROOM_POWER_LEVELS,
            QueryStringValue.IsEmpty
        ))

        val builder = RoomMemberQueryParams.Builder()
        builder.memberships = arrayListOf(Membership.JOIN, Membership.INVITE)
        val query = builder.build()

        mRoom?.let { updateRoomMembers(it.membershipService().getRoomMembers(query)) }

        mRoom?.membershipService()?.getRoomMembersLive(query)
            ?.observe(this, this@GroupDisplayActivity::updateRoomMembers)

        mRoom?.stateService()?.getStateEventLive(STATE_ROOM_POWER_LEVELS,
            QueryStringValue.IsEmpty
        )
            ?.observe(this) { eventOptional: Optional<Event> ->
                updatePowerLevels(eventOptional.get())
            }
    }

    private fun updatePowerLevels(eventPowerLevels: Event?) {
        if (eventPowerLevels == null) return

        val mapLevels = eventPowerLevels.getClearContent()


        val moshi = Moshi.Builder()
            .build()
        moshi.adapter(PowerLevelsContent::class.java).fromJsonValue(mapLevels).let {
            mPlHelper = it?.let { it1 -> PowerLevelsHelper(it1) }
        }

    }

    private fun updateRoomMembers(roomMemberSummaries: List<RoomMemberSummary>) {
        mMembers.clear()

        for (roomMember in roomMemberSummaries) {
            val member = GroupMember(
                username = roomMember.userId,
                nickname = roomMember.displayName,
                role = mPlHelper?.getUserRole(roomMember.userId) ?: Role.Default,
                affiliation = roomMember.membership.name,
                avatarUrl = roomMember.avatarUrl
            )

            if (mSession?.myUserId?.contentEquals(roomMember.userId) == true) {
                mYou = member
            }

            if (canGrantAdmin(member)) {
                mMembers.add(0, member)
            } else {
                mMembers.add(member)
            }
        }

        mBinding.root.adapter?.notifyDataSetChanged()
    }

    private fun inviteContacts(invitees: ArrayList<String>) {
        mCoroutineScope.launch {
            for (invitee in invitees) {
                try {
                    mSession?.getRoom(mRoomId)?.membershipService()?.invite(invitee, mRoomSummary?.topic)

                    val member = GroupMember()
                    val address = BaseAddress(invitee)
                    member.username = address.bareAddress
                    member.nickname = address.user
                    mMembers.add(member)
                } catch (e: Exception) {
                    Timber.e(e, "Error inviting contact $invitee to group.")
                }
            }

            lifecycleScope.launch {
                mBinding.root.adapter?.notifyDataSetChanged()
            }
        }
    }


    fun showMemberInfo(member: GroupMember) {
        if (member === mYou) return

        val canGrantAdmin = canGrantAdmin(mYou)
        val canKickout = canRevokeMembership(mYou)
        //   final boolean isModerator = TextUtils.equals(mYou.role, "moderator")||TextUtils.equals(mYou.role, "admin");

        if (canGrantAdmin || canKickout) {
            val alert = AlertDialog.Builder(this)

            val content = GroupMemberOperationsBinding.inflate(LayoutInflater.from(this))

            val nickname = member.nickname ?: member.username

            content.member.line1.text = nickname
            content.member.line2.text = member.username

            content.member.avatarCrown.visibility =
                if (canGrantAdmin(member)) View.VISIBLE else View.GONE

            if (member.avatarUrl == null) {
                // h.avatar.setImageDrawable(AvatarGenerator.Companion.avatarImage(GroupDisplayActivity.this, 120, AvatarConstants.Companion.getRECTANGLE(), nickname));
                content.member.avatar.visibility = View.VISIBLE
            } else {
                GlideUtils.loadImageFromUri(
                    this,
                    Uri.parse(member.avatarUrl),
                    content.member.avatar,
                    true
                )
            }

            alert.setView(content.root)
            val dialog = alert.show()

            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            if (canGrantAdmin) {
                content.actionMakeAdmin.setOnClickListener {
                    dialog.dismiss()
                    grantAdmin(member)
                }
            } else {
                content.actionMakeAdmin.visibility = View.GONE
            }

            content.actionViewProfile.setOnClickListener {
                dialog.dismiss()
                showMemberProfile(member)
            }

            if (canKickout) {
                content.actionKickout.setOnClickListener {
                    dialog.dismiss()
                    kickout(member)
                }
            } else {
                content.actionKickout.visibility = View.GONE
            }
        } else {
            showMemberProfile(member)
        }
    }

    private fun showMemberProfile(member: GroupMember) {
        startActivity(mApp?.router?.friend(this, member.username))
    }

    private fun grantAdmin(member: GroupMember) {
        try {
            val username = member.username ?: return
            val myUsername = mYou.username ?: return
            val mapLevels =
                mRoom?.stateService()?.getStateEvent(STATE_ROOM_POWER_LEVELS,
                    QueryStringValue.IsEmpty
                )?.getClearContent()
                    ?: return

            val moshi = Moshi.Builder()
                .build()
            val plContent = moshi.adapter(PowerLevelsContent::class.java).fromJsonValue(mapLevels)

      //      val plContent = MoshiProvider.providesMoshi().adapter(PowerLevelsContent::class.java)
        //        .fromJsonValue(mapLevels) ?: return
            val phHelper = plContent?.let { PowerLevelsHelper(it) }
            plContent?.setUserPowerLevel(username, phHelper?.getUserPowerLevelValue(myUsername))

            mCoroutineScope.launch {

                mRoom?.stateService()?.sendStateEvent(STATE_ROOM_POWER_LEVELS, stateKey = "", mapLevels)


                Timber.d("#sendStateEvent success!")
            }
        } catch (ignored: Exception) {
        }
    }

    private fun kickout(member: GroupMember) {
        member.username?.let {
            mCoroutineScope.launch {
                mSession?.getRoom(mRoomId)?.membershipService()?.remove(it, null)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private val mPickContacts =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode != RESULT_OK) return@registerForActivityResult

            var invitees = ArrayList<String>()

            val username = result.data?.getStringExtra(FriendsPickerActivity.EXTRA_RESULT_USERNAME)

            if (username != null) {
                invitees.add(username)
            } else {
                result.data?.getStringArrayListExtra(FriendsPickerActivity.EXTRA_RESULT_USERNAMES)
                    ?.let {
                        invitees = it
                    }
            }

            inviteContacts(invitees)

            mHandler.postDelayed({ updateSession() }, 3000)
        }

    private class HeaderViewHolder(private val mBinding: AwesomeActivityGroupHeaderBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        val avatar get() = mBinding.ivAvatar
        val qr get() = mBinding.qrcode
        val tvRoomName get() = mBinding.tvRoomName
        val ivEditRoomName get() = mBinding.ivEditRoomName
        val tvRoomTopic get() = mBinding.tvRoomTopic
        val ivEditRoomTopic get() = mBinding.iVEditRoomTopic
        val actionShare get() = mBinding.tvActionShare
        val actionAddFriends get() = mBinding.tvActionAddFriends
        val actionNotifications get() = mBinding.tvActionNotifications
        val actionGroupEncryption get() = mBinding.tvActionEncryption
        val checkNotifications get() = mBinding.chkNotifications
        val checkGroupEncryption get() = mBinding.chkGroupEncryption
        val deleteFailedMessage get() = mBinding.tvActionClearMessage
        val clearProgressBar get() = mBinding.clearMessageProgressBar
    }

    private class FooterViewHolder(private val mBinding: AwesomeActivityGroupFooterBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        val actionLeave
            get() = mBinding.tvActionLeave
    }

    private class MemberViewHolder(private val mBinding: GroupMemberViewBinding) :
        RecyclerView.ViewHolder(mBinding.root) {
        val line1 get() = mBinding.line1
        val line2 get() = mBinding.line2
        val avatar get() = mBinding.avatar
        val avatarCrown get() = mBinding.avatarCrown
    }

    private fun showEditAlert(
        title: Int,
        old: CharSequence?,
        callback: (newValue: String) -> Unit
    ) {
        val content = AlertDialogEdittextBinding.inflate(layoutInflater, mBinding.root, false)
        content.editText.setText(old)

        AlertDialog.Builder(this)
            .setTitle(title)
            .setView(content.root)
            .setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->
                callback(content.editText.text.toString())
            }
            .setNegativeButton(R.string.cancel, null)
            .show()
    }

    private fun changeRoomName(name: String) {
        headerViewHolder?.tvRoomName?.text = name

        mCoroutineScope.launch {
            try {
                mRoom?.stateService()?.updateName(name)
            } catch (exception: Throwable) {
                lifecycleScope.launch {
                    headerViewHolder?.tvRoomName?.text = mRoomSummary?.displayNameWorkaround
                }

                throw exception
            }
        }

    }

    private fun changeRoomTopic(topic: String) {
        headerViewHolder?.tvRoomTopic?.text = topic

        mCoroutineScope.launch {
            try {
                mRoom?.stateService()?.updateTopic(topic)
            } catch (exception: Throwable) {
                lifecycleScope.launch {
                    headerViewHolder?.tvRoomTopic?.text = mRoomSummary?.topic
                }

                throw exception
            }
        }
    }

    var isGroupEncryptionEnabled: Boolean
        get() = mRoom?.roomCryptoService()?.isEncrypted() ?: false
        set(enabled) {
            if (enabled) {
                mCoroutineScope.launch {
                    mRoom?.roomCryptoService()?.enableEncryption()
                }
            }
        }

    fun checkNotificationState() {
        val data = mRoom?.roomPushRuleService()?.getLiveRoomNotificationState()
        data?.observe(this) {
            headerViewHolder?.checkNotifications?.isChecked =
                data.value != RoomNotificationState.MUTE
        }
    }

    fun setNotificationsEnabled(enabled: Boolean) {
        mCoroutineScope.launch {
            mRoom?.roomPushRuleService()?.setRoomNotificationState(if (enabled) preferenceProvider.getNotificationState() else RoomNotificationState.MUTE)
        }
    }

    private fun confirmLeaveGroup() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.confirm_leave_group_title))
            .setMessage(getString(R.string.confirm_leave_group))
            .setPositiveButton(getString(R.string.action_leave)) { dialog: DialogInterface, _: Int ->
                dialog.dismiss()
                leaveGroup()
            }
            .setNeutralButton(getString(R.string.action_archive)) { _: DialogInterface?, _: Int -> archiveGroup() }
            .setNegativeButton(android.R.string.cancel, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun archiveGroup() {
        try {
            // TODO archive

            // Clear the stack and go back to the main activity.
            val intent = mApp?.router?.main(this)
            intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

            startActivity(intent)
        } catch (e: Exception) {
            Timber.e(e, "error leaving group")
        }
    }

    private fun leaveGroup() {
        mCoroutineScope.launch {
            try {
                mRoom?.roomId?.let { mSession?.roomService()?.leaveRoom(it,null) }
            } catch (e: Exception) {
                Timber.e(e, "error leaving group")
            }
        }

        // Clear the stack and go back to the main activity.
        val intent = mApp?.router?.main(this)
        intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

        startActivity(intent)
    }

    private fun showAddFriends() {
        if (mActionAddFriends == null) return

        if (!canInviteOthers(mYou)) {
            mActionAddFriends?.visibility = View.GONE
            mActionShare?.visibility = View.GONE
        } else {
            mActionAddFriends?.setOnClickListener {
                val usernames = mMembers.map { it.username } as ArrayList<String?>

                mPickContacts.launch(
                    mApp?.router?.friendsPicker(
                        this@GroupDisplayActivity,
                        usernames
                    )
                )
            }

            mActionShare?.visibility = View.VISIBLE
            mActionAddFriends?.visibility = View.VISIBLE
            mActionAddFriends?.isEnabled = true
        }
    }

    private fun canChangeName(member: GroupMember?): Boolean {
        return member?.role is Role.Admin || member?.role is Role.Moderator
    }

    private fun canInviteOthers(member: GroupMember?): Boolean {
        return canChangeName(member)
    }

    fun canGrantAdmin(granter: GroupMember?): Boolean {
        return granter?.role is Role.Admin || granter?.role is Role.Moderator
    }

    private fun canRevokeMembership(revoker: GroupMember?): Boolean {
        return canGrantAdmin(revoker)
    }

    private fun setAvatar(bmp: Bitmap) {
        headerViewHolder?.avatar?.setImageDrawable(BitmapDrawable(resources, bmp))

        mCoroutineScope.launch {
            @Suppress("BlockingMethodInNonBlockingContext")
            val file = File.createTempFile("avatar", "jpg")

            file.outputStream().use { os ->
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, os)
            }

            mRoom?.stateService()?.updateAvatar(Uri.fromFile(file), file.name)

            file.delete()
        }
    }

    /**
     * Creates an alias for the room and sets the first alias of all aliases found as the canonical
     * alias. In other words: This should only be used, if the room has no alias, yet.
     */
    private fun createAlias() {
        lifecycleScope.launch {
            val content = AlertDialogEdittextBinding.inflate(layoutInflater, mBinding.root, false)

            AlertDialog.Builder(this@GroupDisplayActivity)
                .setTitle(R.string.Create_Published_Address)
                .setMessage(R.string.To_be_able_to_share_a_room_)
                .setView(content.root)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->
                    val alias = content.editText.text.toString()

                    if (alias.isBlank()) return@setPositiveButton

                    mCoroutineScope.launch {
                        mRoom?.aliasService()?.addAlias(alias)

                        mRoom?.aliasService()?.getRoomAliases()?.let {
                            // We take the first, that should be the only one. We shouldn't be here
                            // otherwise. (Or someone changed the call logic...)
                            mRoom?.stateService()?.updateCanonicalAlias(it.firstOrNull(), it)
                            showShareSheet(it.first())
                        }
                    }
                }
                .show()
        }
    }

    /**
     * Checks, if the room is set public, and if not tries to set so.
     *
     * When done, creates a QR code and a link to share, so other people can be invited to this
     * room via some other messenger or the likes.
     */
    private fun showShareSheet(alias: String) {

        @Suppress("BlockingMethodInNonBlockingContext")
        val inviteLink = OnboardingManager.generateInviteLink(URLEncoder.encode(alias, "UTF-8"))

        mCoroutineScope.launch {
            if (mRoom?.stateService()?.isPublic() == false) {

                // Set room to public, so users with link can actually join, but not guests!
                mRoom?.stateService()?.updateJoinRule(RoomJoinRules.PUBLIC, GuestAccess.Forbidden)
            }

            QrShareAsyncTask().shareQrCode(this@GroupDisplayActivity, inviteLink)
        }
    }
}