package info.guardianproject.keanuapp.ui.conversation;

import android.view.View;

import info.guardianproject.keanu.core.ImApp;
import info.guardianproject.keanu.core.ui.room.StoryActivity;
import info.guardianproject.keanu.core.ui.room.StoryView;
import info.guardianproject.keanuapp.R;

import static info.guardianproject.keanu.core.ui.room.RoomActivity.REQUEST_ADD_MEDIA;

/**
 * Created by N-Pex on 2019-04-12.
 */
public class StoryViewContrib extends StoryView {

    public StoryViewContrib(StoryActivity activity, String roomId) {
        super(activity, roomId);
        View btnAdd = activity.findViewById(R.id.btnAddNew);
        btnAdd.setOnClickListener(v -> activity.startActivityForResult(((ImApp) activity.getApplication()).getRouter().addUpdateMedia(activity),
                REQUEST_ADD_MEDIA));
    }



}
