package info.guardianproject.keanuapp.ui.qr;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import info.guardianproject.keanuapp.ui.qr.zxing.encode.Contents;
import info.guardianproject.keanuapp.ui.qr.zxing.encode.QRCodeEncoder;

public class QrGenAsyncTask extends AsyncTask<String, Void, Void> {
    private static final String TAG = "QrGenAsyncTask";
    private ImageView view;
    private Bitmap qrBitmap;
    private int qrCodeDimension;

    public QrGenAsyncTask(ImageView view, int qrCodeDimension) {
        this.view = view;
        this.qrCodeDimension = qrCodeDimension;
    }

    /*
     * The method for getting screen dimens changed, so this uses both the
     * deprecated one and the 13+ one, and supports all Android versions.
     */
    @SuppressWarnings("deprecation")
    @TargetApi(13)
    @Override
    protected Void doInBackground(String... s) {
        String qrData = s[0];
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrData, null,
                Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(), qrCodeDimension);

        try {
            qrBitmap = qrCodeEncoder.encodeAsBitmap();
        } catch (WriterException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        Log.v(TAG, qrBitmap.toString());
        if (view != null) {
            view.setImageBitmap(qrBitmap);
        }
    }
}