package info.guardianproject.keanuapp.ui.widgets;

import android.view.View;

import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;

import java.io.IOException;
import java.util.Objects;

import info.guardianproject.keanu.core.ui.widgets.VideoViewActivity;
import info.guardianproject.keanuapp.R;

/**
 * Created by N-Pex on 2019-04-29.
 */
public class StoryExoPlayerManager {

    public static void recordAudio(AudioRecorder audioRecorder, StyledPlayerView view) {
        view.setVisibility(View.VISIBLE);
        VisualizerView visualizerView = view.findViewById(R.id.exo_visualizer_view);
        if (visualizerView != null) {
            visualizerView.reset();
            visualizerView.setVisibility(View.VISIBLE);
            audioRecorder.setVisualizerView(visualizerView);
        }
    }

    public static void stop(StyledPlayerView view) {
        if (view.getPlayer() != null) {
            view.getPlayer().setPlayWhenReady(false);
        }
    }

    public static void load(MediaInfo mediaInfo, StyledPlayerView view, boolean play) {
        VisualizerView visualizerView = view.findViewById(R.id.exo_visualizer_view);
        visualizerView.reset();

        // Had the player been setup?
        if (view.getPlayer() == null) {
            SimpleExoPlayer exoPlayer = new SimpleExoPlayer.Builder(view.getContext()).build();

            // Bind to views
            visualizerView.setExoPlayer(exoPlayer);
            view.setPlayer(exoPlayer);
        }

        if (mediaInfo.isAudio()) {
            visualizerView.loadAudioFile(mediaInfo.uri);
            visualizerView.setVisibility(View.VISIBLE);
        } else {
            visualizerView.setVisibility(View.GONE);
        }

        Objects.requireNonNull(view.getVideoSurfaceView()).setAlpha(mediaInfo.isAudio() ? 0 : 1);

        DataSpec dataSpec = new DataSpec(mediaInfo.uri);
        final VideoViewActivity.InputStreamDataSource inputStreamDataSource = new VideoViewActivity.InputStreamDataSource(view.getContext(), dataSpec);
        try {
            inputStreamDataSource.open(dataSpec);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DataSource.Factory factory = () -> inputStreamDataSource;

        MediaSource mediaSource = new ProgressiveMediaSource.Factory(factory)
                .createMediaSource(MediaItem.fromUri(inputStreamDataSource.getUri()));

        //LoopingMediaSource loopingSource = new LoopingMediaSource(mediaSource);
        SimpleExoPlayer exoPlayer = (SimpleExoPlayer) view.getPlayer();
        exoPlayer.setMediaSource(mediaSource);
        exoPlayer.prepare();

        if (play) {
            exoPlayer.setPlayWhenReady(true); // Run file/link when ready to play.
        }
    }
}
