package info.guardianproject.keanuapp.viewmodel

import android.graphics.BitmapFactory
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.BitmapHelper.saveAsJPG
import info.guardianproject.keanu.core.util.SecureMediaStore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException

class MediaProcessViewModel: ViewModel() {
    private val EXTENSION_JPG = ".jpg"

    private val mSaveMediaLiveData = MutableLiveData<String?>()
    val observableSaveMediaLiveData = mSaveMediaLiveData as LiveData<String?>

    fun saveMediaToGallery(mimeType: String, fileUri: Uri, saveToExternalStorage: Boolean) {

        GlobalScope.launch(Dispatchers.IO) {
            val appContext = ImApp.sImApp?.applicationContext
            appContext?.let {
                val exportPath = SecureMediaStore.exportPath(appContext, mimeType, fileUri, saveToExternalStorage)
                try {
                    SecureMediaStore.exportContent(mimeType, fileUri, exportPath)
                    val imageBmp = BitmapFactory.decodeFile(exportPath.absolutePath)
                    imageBmp.saveAsJPG(exportPath.name)
                    mSaveMediaLiveData.postValue(exportPath.name)
                } catch (e: IOException) {
                    Timber.e(e)
                    mSaveMediaLiveData.postValue(null)
                }
            }
        }
    }
}